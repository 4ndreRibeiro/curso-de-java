### Capitulo 1 - Introdução ao Java

###### A Origem

**Java** é o ambiente computacional, ou **plataforma**, criada pela empresa estadunidense **Sun Microsystems**, e vendida para a Oracle depois de alguns anos. A plataforma permite desenvolver programas utilizando a linguagem de programação Java. Uma grande vantagem da plataforma é a de não estar presa a um único sistema operacional ou *hardware*, pois seus programas rodam através de uma máquina virtual que pode ser emulada em qualquer sistema que suporte a [linguagem C++.
Java é uma linguagem de programação orientada a objetos desenvolvida na década de 90 por uma equipe de programadores chefiada por James Gosling, na empresa Sun Microsystems. Em 2008 o Java foi adquirido pela empresa Oracle Corporation. Diferente das linguagens de programação modernas, que são compiladas para código nativo, a linguagem Java é compilada para um bytecode que é interpretado por uma máquina virtual (Java Virtual Machine, mais conhecida pela sua abreviação JVM). A linguagem de programação Java é a linguagem convencional da Plataforma Java, mas não é a sua única linguagem. J2ME Para programas e jogos de computador, celular, calculadoras, ou até mesmo o rádio do carro.

###### O caminho para aprendizagem

O caminho para aprendizagem do Java, por onde começar a programar em Java.
Afinal de contas, quando começa a aprender a linguagem, os caminhos e possibilidades são infinitos e ter uma orientação para dar aquele start é fundamental para os profissionais que querem investir tempo em seu aprendizado.
Por isso posso citar quatro caminhos para você começar os estudos.
Comece com os conceitos básicos;
Domine a linguagem de programação;
Aprenda tudo sobre a programação orientada a objetos (POO);
Aprenda inglês.
Dominando bem esses conceitos, você conseguirá avançar na linguagem sem grandes dificuldades.
De modo mais subjetivo, posso citar ainda o foco para a resolução de problemas e o gosto por desafios e novidades.
Não é novidade para ninguém que desenvolvedores precisam gostar de resolver problemas.
Afinal de contas, este é um dos principais objetivos ao qual uma empresa contrata um programador.
Por isso você precisa ter uma mente analítica e buscar resolver o máximo de problemas com o Java, essa é, inclusive, uma das formas de praticar o que acabara de aprender.
O gosto por desafios deve ser outro detalhe que deve te acompanhar nesse caminho de aprendizado do Java ou qualquer outra linguagem de programação.
São os desafios que te farão avançar e é através deles que você vai crescer.

###### Java como plataforma

O universo Java é um vasto conjunto de tecnologias, composto por três plataformas principais que foram criadas para segmentos específicos de aplicações:

- **Java SE** (Java Platform, Standard Edition). É a base da plataforma. Inclui o ambiente de execução e as bibliotecas comuns.
- **Java EE** (Java Platform, Enterprise Edition). A edição voltada para o desenvolvimento de aplicações corporativas e para internet.
- **Java ME** (Java Platform, Micro Edition). A edição para o desenvolvimento de aplicações para dispositivos móveis e embarcados.

Além disso, pode-se destacar outras duas plataformas Java mais específicas:

- **Java Card**. Voltada para dispositivos embarcados com limitações de processamento e armazenamento, como smart cards e o Java Ring.
- **JavaFX**. Plataforma para desenvolvimento de aplicações multimídia em desktop/web (JavaFX Script) e dispositivos móveis (JavaFX Mobile).

###### Finalidade

Um programa escrito para a plataforma Java necessita de dois componentes para ser executado: a máquina virtual Java, e um conjunto de bibliotecas de classe que disponibilizam um série de serviços para esse programa. O pacote de *software* que contém a máquina virtual e esta biblioteca de classes é conhecido como JRE (Java Runtime Environment).

**Java Virtual Machine**

O coração da plataforma Java é o conceito de um processador "virtual", que executa os programas formados por *bytecodes* Java. Este *bytecode* é o mesmo independentemente do *hardware* ou sistema operacional do sistema em que o programa será executado. A plataforma Java disponibiliza um interpretador, a JVM, que traduz, em tempo de execução, o *bytecode* para instruções nativas do processador. Isto permite que uma mesma aplicação seja executada em qualquer plataforma computacional que possua uma implementação da máquina virtual.

Desde a versão 1.2 da JRE, a implementação da Sun da JVM inclui um compilador *just-in-time* (JIT). Com este compilador todo o *bytecode* de um programa é transformado em instruções nativas e carregado na máquina virtual em uma só operação, permitindo um ganho de desempenho muito grande em comparação com a implementação anterior, onde as instruções em *bytecode* eram interpretadas uma por vez. O compilador JIT pode ser projetado de acordo com a plataforma ou *hardware* de destino, e o código que ele gera pode ser otimizado com base na observação de padrões de comportamento dos programas.

Desde a primeira versão, este ambiente de execução vem equipado com gestão automática de memória, realizada por um algoritmo coletor de lixo que liberta o programador das tarefas de alocação e libertação de memória, fonte de muitos erros de programação.

A plataforma Java não é a primeira plataforma baseada em uma máquina virtual, mas é de longe a mais conhecida e a que alcançou maior sucesso. Anteriormente esta tecnologia era utilizada na criação de emuladores para auxílio ao projeto de *hardware* ou de sistemas operacionais. A plataforma Java foi desenhada para ser implementada inteiramente em *software*, enquanto permitindo a sua migração de maneira fácil para plataformas de *hardware* de todos os tipos.

###### Características

A linguagem Java foi projetada tendo em vista os seguintes objetivos:
Orientação a objetos - Baseado no modelo de Simular;
Portabilidade - Independência de plataforma - "escreva uma vez, execute em qualquer lugar" ("write once, run anywhere");
Recursos de Rede - Possui extensa biblioteca de rotinas que facilitam a cooperação com protocolos TCP/IP, como HTTP e FTP;
Segurança - Pode executar programas via rede com restrições de execução.
Além disso, podem-se destacar outras vantagens apresentadas pela linguagem:
Sintaxe similar a C/C++
Facilidades de Internacionalização - Suporta nativamente caracteres Unicode;
Simplicidade na especificação, tanto da linguagem como do "ambiente" de execução (JVM);
É distribuída com um vasto conjunto de bibliotecas (ou APIs);
Possui facilidades para criação de programas distribuídos e multitarefa (múltiplas linhas de execução num mesmo programa);
Desalocação de memória automática por processo de coletor de lixo;
Carga Dinâmica de Código - Programas em Java são formados por uma coleção de classes armazenadas independentemente e que podem ser carregadas no momento de utilização.

###### Plataformas similares

O sucesso da plataforma Java e o seu conceito *write once, run anywhere* (escreva uma vez, execute em qualquer lugar) levaram a outros esforços similares. O mais notável destes esforços é a plataforma **.NET**, da **Microsoft**, que utilizou muitos dos conceitos e inovações da plataforma Java sem, contudo, implementar os recursos de portabilidade entre sistemas operacionais e plataformas que a plataforma Java possui.

###### Exercícios

### Capitulo 2 - Ambiente de Desenvolvimento

###### Kits de Ferramentas

**Kit essencial para compilar e executar programas em Java**

Não existe opção. Antes mesmo de começar a programar em Java, você precisa instalar o Java SE Development Kit na máquina. O pacote traz todos os recursos imprescindíveis para desenvolver aplicações e applets Java.

Quem navega na internet já conhece a incrível variedade de possibilidades que o Java proporciona. Desde simples instruções a jogos cheios de cores e interatividade, a linguagem da Sun está por trás de centenas de diferentes tipos de recursos que encontramos na web.**Java Virtual Machine**

O Java SE Development Kit inclui o compilador e as bibliotecas (APIs) necessárias para programar todo o tipo de aplicações. O pacote adiciona também a máquina virtual Java ao sistema operacional se você ainda não tiver instalado o recurso no computador.

Outro item incluído no Java SE Development Kit cuida automaticamente da declaração e comentários das classes, interfaces e métodos utilizados na programação. O Javadoc é configurável e produz uma série de páginas HTML com uma descrição detalhada dos arquivos fonte.

###### Instalação do JDK no Windows

Para dar início ao desenvolvimento de aplicações em Java, é necessário ter o JDK instalado no ambiente operacional. Para quem ainda utiliza o Windows, este artigo contém as instruções para a instalação e configuração do JDK.

1. Baixando o JDK
   Visite a [página de download do JDK no site da Oracle](https://www.oracle.com/java/technologies/javase-downloads.html) e clique no link do JDK, destacado na imagem a seguir:

   ![](/home/andre/Downloads/oracle17ubuntu.jpeg)

**Como configurar as variáveis de ambiente do Java no Windows 10?**

***\*Windows 10\**

1. Em Pesquisar, procure e selecione: Sistema (Painel de Controle)

2. Clique no link Configurações avançadas do sistema.

3. Clique em **Variáveis de Ambiente**. ...

4. Na janela Editar **Variável** de Sistema (ou Nova **Variável** de Sistema), especifique o valor da **variável de ambiente** PATH .

   **configurar variáveis de ambiente do Java?***
   *Para \**configurar\** a \**variável\** JAVA_HOME:**

   1. Localize o diretório de instalação **Java**. ...
   2. Faça um dos seguintes: ...
   3. Clique no botão **Variáveis de ambiente**.
   4. Sob **Variáveis** do sistema, clique em Novo.
   5. No campo Nome da **variável** insira: ...
   6. No campo Valor da **variável**, insira o seu caminho da instalação do JDK ou JRE.

**Ambiente de tempo de Execução Java?**

Se você instalou o ambiente de tempo de execução Java (JRE), deverá seguir os mesmos passos, mas configurar a variável de ambiente JRE_HOME . Para configurar a variável JAVA_HOME:

Imagens

###### Instalação do JDK no Linux

![](/home/andre/Downloads/oracle17.jpeg)

O primeiro passo é baixar o JDK. Acesse a [página de downloads do Java SE](http://www.oracle.com/technetwork/java/javase/downloads/), aceite o contrato de licença e escolha fazer download do arquivo **.tar.gz** que contém o JDK compactado para Linux. Uma vez extraído o conteúdo desse arquivo, o JDK já pode ser executado e deve funcionar em qualquer distribuição Linux:

**Extração**

No Linux, o processo de instalação do JDK se resume a extrair o conteúdo do arquivo baixado em um diretório cuja localização seja acessível a todos os usuários do sistema (como o diretório “/opt”). Para realizar esse procedimento, é necessário possuir privilégios de administrador.

Mova o arquivo baixado (no exemplo, “[jdk-17_linux-x64_bin.tar.gz"](https://download.oracle.com/java/17/latest/jdk-17_linux-x64_bin.tar.gz) para a pasta de destino (“/opt”) e nela extraia seu conteúdo. Nesse exemplo, o caminho para o JDK será então “/opt/jdk-17_linux-x64″. Após a extração, o arquivo baixado pode ser excluído. Os comandos de terminal para realizar esses procedimentos são:

```
# mv /home/soulcode/jdk-17_linux-x64.tar.gz /opt
# cd /opt
# tar zxvf jdk-17_linux-x64_bin.tar.gz
# rm jdk-17_linux-x64.tar.gz
```

**Configuração das variáveis de ambiente JAVA_HOME e PATH**

A variável de ambiente JAVA_HOME é consultada por alguns programas (como o Maven, o Eclipse e o JBoss Application Server, por exemplo) a fim de obter a localização do JDK instalado no computador.

Também é costume acrescentar o caminho para o JDK na variável de ambiente PATH, possibilitando invocar as ferramentas do JDK de qualquer diretório. Do contrário, deve-se informar o caminho completo para essas ferramentas todas as vezes em que for necessário invocá-las.

Abra com o seu editor de preferência o arquivo “/etc/profile” e acrescente as seguintes linhas no final (você deve substituir, se necessário, a localização do JDK):

```
export JAVA_HOME="/opt/jdk-17_linux-x64"
export CLASSPATH="$JAVA_HOME/lib"
export PATH="$PATH:$JAVA_HOME/bin"
export MANPATH="$MANPATH:$JAVA_HOME/man"
```

As alterações serão aplicadas a qualquer nova janela de terminal aberta.

**Teste do JDK**

Para testar a instalação do JDK, abra uma janela de terminal e execute o comando a seguir, que deve informar a versão do JDK instalado no seu computador:

```
java -version 
javac -version
```

###### Exercícios

### Capítulo 3 - Iniciando no Java

###### Introdução a linguagem java

Java é muito fácil de aprender, e sua sintaxe é simples, limpa e fácil de entender. De acordo com a Sun Microsystem, a linguagem Java é uma linguagem de programação simples porque:

- A sintaxe Java é baseada em C++ (tão mais fácil para os programadores aprenderem depois de C++).
- Java removeu muitos recursos complicados e raramente usados, por exemplo, ponteiros explícitos, sobrecarga do operador, etc.
- Não há necessidade de remover objetos não-ferenciados porque há uma coleta automática de lixo em Java.

**Características mais importantes do Java**

- Simples
- Orientado a objetos
- Portátil
- Plataforma independente
- Seguro
- Robusto
- Arquitetura neutra
- Interpretado
- Alto Desempenho
- Multithread
- Distribuído
- Dinâmico

**Orientado a objetos**
Java é uma linguagem de programação orientada a objetos. Tudo em Java é um objeto. Orientado a objetos significa que organizamos nosso software como uma combinação de diferentes tipos de objetos que incorporam dados e comportamento.

A programação orientada a objetos (OOPs) é uma metodologia que simplifica o desenvolvimento e a manutenção de software, fornecendo algumas regras.

Os conceitos básicos de OOPs são:

- Objeto
- Classe
- Herança
- Polimorfismo
- Abstração
- Encapsulação

**Plataforma Independente**
Java é independente de plataforma porque é diferente de outros idiomas como C, C++, etc. que são compilados em máquinas específicas da plataforma, enquanto Java é uma gravação uma vez, executada em qualquer idioma. Uma plataforma é o ambiente de hardware ou software no qual um programa é executado.

Existem dois tipos de plataformas baseadas em software e baseadas em hardware. Java fornece uma plataforma baseada em software.

A plataforma Java difere da maioria das outras plataformas no sentido de que é uma plataforma baseada em software que é executada em cima de outras plataformas baseadas em hardware. Tem dois componentes:

- Ambiente de tempo de execução
- API (Interface de Programação de Aplicativos)

O código Java pode ser executado em várias plataformas, por exemplo, Windows, Linux, Sun Solaris, Mac/OS, etc. O código Java é compilado pelo compilador e convertido em bytecode. Este bytecode é um código independente da plataforma porque pode ser executado em várias plataformas, ou seja, **Write Once and Run Anywhere (WORA)**.

**Seguro**

Java é mais conhecido por sua segurança. Com Java, podemos desenvolver sistemas livres de vírus. Java é protegido porque:

- **Sem ponteiro explícito**
- **Programas Java são executados dentro de uma caixa de areia de máquina virtual**

- **Carregador de classe:** O Classloader em Java faz parte do Java Runtime Environment (JRE), que é usado para carregar classes Java na Máquina Virtual Java dinamicamente. Ele adiciona segurança separando o pacote para as classes do sistema de arquivos local daqueles que são importados de fontes de rede.
- **Verificador de bytecode:** Verifica os fragmentos de código para código ilegal que podem violar os direitos de acesso a objetos.
- **Gerente de segurança:** Ele determina quais recursos uma classe pode acessar, como ler e escrever para o disco local.

A linguagem Java fornece esses títulos por padrão. Alguma segurança também pode ser fornecida por um desenvolvedor de aplicativos explicitamente através de SSL, JAAS, Criptografia, etc.

**Robusto**

A mineração inglesa de Robust é forte. Java é robusto porque:

- Ele usa um forte gerenciamento de memória.
- Há uma falta de ponteiros que evita problemas de segurança.
- Java fornece coleta automática de lixo que funciona na Máquina Virtual Java para se livrar de objetos que não estão mais sendo usados por um aplicativo Java.
- Há tratamento de exceção e o mecanismo de verificação de tipo em Java. Todos esses pontos tornam Java robusto.

**Arquitetura neutra**

Java é neutro em arquitetura porque não há características dependentes de implementação, por exemplo, o tamanho dos tipos primitivos é fixo.

Na programação C, o tipo de dados int ocupa 2 bytes de memória para arquitetura de 32 bits e 4 bytes de memória para arquitetura de 64 bits. No entanto, ele ocupa 4 bytes de memória para arquiteturas de 32 e 64 bits em Java.

**Portátil**

Java é portátil porque facilita que você carregue o código Java para qualquer plataforma. Não requer nenhuma implementação.

**Alto desempenho**

Java é mais rápido do que outras linguagens tradicionais de programação interpretadas porque o bytecode Java está "perto" do código nativo. Ainda é um pouco mais lento do que uma linguagem compilada (por exemplo, C++). Java é uma linguagem interpretada que é por isso que é mais lenta do que as línguas compiladas, por exemplo, C, C++, etc.

**Distribuído**

Java é distribuído porque facilita os usuários a criar aplicativos distribuídos em Java. RMI e EJB são usados para criar aplicativos distribuídos. Esse recurso do Java nos torna capazes de acessar arquivos ligando para os métodos de qualquer máquina na internet.

**Multi-thread**

Um fio é como um programa separado, executando simultaneamente. Podemos escrever programas Java que lidam com muitas tarefas ao mesmo tempo definindo vários segmentos. A principal vantagem do multi-threading é que ele não ocupa a memória para cada segmento. Ele compartilha uma área de memória comum. Os threads são importantes para várias mídias, aplicativos web, etc.

**Dinâmico**

Java é uma linguagem dinâmica. Suporta o carregamento dinâmico das aulas. Significa que as aulas estão carregadas sob demanda. Ele também suporta funções de suas línguas nativas, ou seja, C e C++.

Java suporta compilação dinâmica e gerenciamento automático de memória (coleta de lixo).

**C++ vs Java**

Existem muitas diferenças e semelhanças entre a linguagem de programação C++ e Java. Uma lista das principais diferenças entre C++ e Java são dadas abaixo:

|              Índice de Comparação              |                             C++                              |                             Java                             |
| :--------------------------------------------: | :----------------------------------------------------------: | :----------------------------------------------------------: |
|         **Independente da plataforma**         |              O C++ é dependente da plataforma.               |              Java é independente de plataforma.              |
|         **Usado principalmente para**          |   C++ é usado principalmente para programação de sistemas.   | Java é usado principalmente para programação de aplicativos. É amplamente utilizado em aplicativos baseados no Windows, baseados na Web, corporativos e móveis. |
|             **Objetivo de design**             | O C++ foi projetado para programação de sistemas e aplicações. Era uma extensão da **linguagem de programação C**. | Java foi projetado e criado como um intérprete para sistemas de impressão, mas mais tarde estendido como uma computação de rede de suporte. Foi projetado para ser fácil de usar e acessível a um público mais amplo. |
|                    **Goto**                    |              C++ suporta a instrução **goto**.               |              Java não suporta a instrução goto.              |
|              **Herança múltipla**              |                C++ suporta herança múltipla.                 | Java não suporta herança múltipla através da classe. Ele pode ser alcançado usando **interfaces em java**. |
|           **Sobrecarga do operador**           |             C++ suporta sobrecarga do operador.              |           Java não suporta sobrecarga do operador.           |
|                 **Ponteiros**                  | C++ suporta **ponteiros**. Você pode escrever um programa de ponteiros em C++. | Java suporta ponteiro internamente. No entanto, você não pode escrever o programa de ponteiros em java. Significa que java tem suporte restrito ao ponteiro em java. |
|          **Compilador e Intérprete**           | C++ usa apenas compilador. C++ é compilado e executado usando o compilador que converte código-fonte em código de máquina, então, C++ é dependente da plataforma. | Java usa compilador e intérprete. O código-fonte Java é convertido em bytecode no momento da compilação. O intérprete executa este bytecode em tempo de execução e produz saída. Java é interpretado é por isso que é independente da plataforma. |
| **Chamada por Valor e Chamada por referência** |   C++ suporta chamada por valor e chamada por referência.    | Java suporta chamada apenas por valor. Não há chamada por referência em java. |
|             **Estrutura e União**              |             O C++ apoia estruturas e sindicatos.             |          Java não suporta estruturas e sindicatos.           |
|              **Suporte a Thread**              | C++ não tem suporte embutido para threads. Ele conta com bibliotecas de terceiros para suporte a threads. |             Java tem suporte de Thread embutido.             |
|         **Comentário de documentação**         |           C++ não suporta comentários documentais.           | Java suporta comentário de documentação (/** ... */) para criar documentação para código-fonte java. |
|           **Palavra-chave virtual**            | C++ suporta palavra-chave virtual para que possamos decidir se anulamos ou não uma função. | Java não tem palavra-chave virtual. Podemos substituir todos os métodos não estáticos por padrão. Em outras palavras, métodos não estáticos são virtuais por padrão. |
|     **>>> de turno direito não assinado**      |                C++ não suporta operador >>>.                 | Java suporta o operador de >>> de turno direito não assinado que preenche zero no topo para os números negativos. Para números positivos, funciona como >> operador. |
|             **Árvore de Herança**              |         C++ sempre cria uma nova árvore de herança.          | Java sempre usa uma única árvore de herança porque todas as classes são o filho da classe Objeto em Java. A classe Objeto é a raiz da árvore de **herança** em java. |
|                  **Hardware**                  |              C++ está mais próximo do hardware.              |           Java não é tão interativo com hardware.            |
|            **Orientado a objetos**             | C++ é uma linguagem orientada a objetos. No entanto, na língua C, uma única hierarquia raiz não é possível. | Java também é uma linguagem **orientada a objetos**. No entanto, tudo (exceto tipos fundamentais) é um objeto em Java. É uma única hierarquia raiz, pois tudo é derivado de java.lang.Object. |

###### Primeiro programa em Java

Nesta seção, aprenderemos a escrever o programa simples de Java. Podemos escrever um simples programa Hello Java facilmente depois de instalar o JDK.

Para criar um programa Java simples, você precisa criar uma classe que contenha o método principal. Vamos entender o requisito primeiro.

**Criando o exemplo do Olá Mundo**

Vamos criar o programa Hello Java:

```
public class HelloJava {
    public static void main(String args[]) {
        System.out.println("Olá Java");
    }
}
```

Salve o arquivo acima como HelloJava.java.

**Para compilar:**
javac HelloJava.java
**Para executar:**
java HelloJava

**Saída:**

```
Olá Java
```

**Fluxo de compilação:**
Quando compilamos o programa Java usando a ferramenta javac, o compilador Java converte o código-fonte em código de byte.

![](/home/andre/Downloads/copilador.jpeg)

| **HelloJava.java** | HelloJava.class |
| ------------------ | --------------: |

**No tempo de execução, são realizadas as seguintes etapas:**

<img src="/home/andre/Downloads/java-runtime-processing.png" style="zoom: 80%;" />

**Classloader**: É o subsistema do JVM que é usado para carregar arquivos de classe.
**Bytecode Verifier:** Verifica os fragmentos de código para código ilegal que podem violar os direitos de acesso a objetos.
**Interpreter:** Leia o fluxo de bytecode e execute as instruções.

**Parâmetros utilizados no Programa Primeiro Java**

Vamos ver qual é o significado de class, public, static, void, main, String[], System.out.println().

- **class** é usado para declarar uma classe em Java.
- **public** é um modificador de acesso que representa visibilidade. Significa que é visível para todos.
- **static** é uma palavra-chave. Se declararmos qualquer método como estático, é conhecido como método estático. A principal vantagem do método estático é que não há necessidade de criar um objeto para invocar o método estático. O método principal() é executado pelo JVM, por isso não requer a criação de um objeto para invocar o método principal(). Então, salva a memória.
- **void** é o tipo de retorno do método. Significa que não devolve nenhum valor.
- **main* representa o ponto de partida do programa.
- String[] args ou String args[] é usado para **argumento de linha de comando**. Discutiremos isso na próxima seção.
- **System.out.println()** é usado para imprimir declaração. Aqui, System é uma classe, out é um objeto da classe PrintStream, println() é um método da classe PrintStream. Discutiremos o funcionamento interno da instrução System.out.println() na próxima seção.

###### Saída de Dados no Modo Textual

###### Impressão de Valores Literais

###### Saída Formatadas

###### Entrada de Dados no Modo Textual

###### Entrada e Saída de Dados com Diálogos Gráficos

###### Exercícios

### Capítulo 4 - Variáveis e Constantes

###### Tipos de Dados

Tipos de dados são especificados de diferentes tamanhos e valores que podem ser armazenados na variável. Existem dois tipos de dados em java:

1. **Tipo de dado primitivo:** Incluem boolean, char, byte, short, int, long, float, e double.
2. **Tipo de dado não primitivo:** Incluem classes, interfaces e arrays.

**Tipos de dado primitivo em java**

Na linguagem java, tipo de dado primitivo são como blocos de construção que nos permitem manipular os dados. Existem muitos tipos de data básico disponível na linguagem java.

Existem oito tipos de tipo de dado primitivo:

- **Tipo boolean**

  O tipo de dado boolean é usado para armazenar somente dois possíveis valores: verdadeiro e falso. Este tipo de dado é usado por sinalizadores simples que localizam condições verdadeira/falsa.

  Especifica um bit de informação, mas o "tamanho" não pode ser especificado precisamente.

  **Exemplo:** boolean one = false

- **Tipo byte**

  É um exemplo de um tipo de dado primitivo. É um inteiro de complemento de dois com assinatura de 8 bits. Sua faixa de valor está entre -128 até 127 (inclusive). Valor mínimo é -128 e o valor máximo é 127. Seu valor padrão é 0 (zero).

  O tipo de dado byte é usado para salvar em memória grandes matrizes onde a memória salva é muito requisitada. Isso economiza espaço porque um byte é quatro vezes menor que um número inteiro. Também pode ser usado no lugar de tipos de dados "int".

  **Exemplo:** byte a = 10, byte b = -20

  **Tipo char**

  O tipo de dado char é um único caractere unicode de 16 bits. Sua faixa de valor está entre '\u0000' (ou 0) até '\uffff' (ou 65.535 inclusive). O tipo de data char é usado para para armazenar caracteres.

  **Exemplo:** char letraA = 'A'

  **Tipo short**

  O tipo de dado short é um inteiro de complemento de dois com assinatura de 16 bits. Sua faixa de valor está entre -32.768 até 32.767 (inclusive). Seu valor mínimo é -32.768 e valor máximo é 32.767. Seu valor padrão é 0 (zero).

  O tipo de data short também pode ser usado para economizar memória assim como o tipo de dado byte. Um tipo de dado short é duas vezes menor que um inteiro.

  **Exemplo:** short s = 10000, short r = -5000

- **Tipo int**

  O tipo de dado int é um inteiro de complemento de dois com assinatura de 32 bits. Sua faixa de valor está entre -2.147.483.648 (-2³¹) até 2.147.483.647 (2³¹?¹) (inclusive). Seu valor mínimo é -2.147.483.648 e valor máximo é 2.147.483.647. Seu valor padrão é 0 (zero).

  O tipo de dado int geralmente é usado como um tipo de dado padrão para valores inteiros sem nenhum problema de memória.

  **Exemplo:** int a = 100000, int b = -200000

- **Tipo long**

  O tipo de dado long é um inteiro de complemento de dois com assinatura de 64 bits. Sua faixa de valor está entre -9.223.372.036.854.775.808 (-2?³) até 9.223.372.036.854.775.807 (2?³?¹) (inclusive). Seu valor mínimo é -9.223.372.036.854.775.808 e o valor máximo é de 9.223.372.036.854.775.807. O valor padrão é 0 (zero). O tipo de dado long é usado quando você precisa de uma faixa de valores maior do que a fornecida por int.

  **Exemplo:** long a = 100000L, long b = -200000L

- **Tipo float**

  O tipo de dado float é um ponto flutuante IEEE 754 de 32 bits de precisão única. Sua faixa de valor é ilimitada. É recomendado usar um ponto flutuante (em vez de double) se você precisar economizar memória em grandes arrays (matrizes) de número de ponto flutuante. O tipo de dado float nunca deve ser usado para valores precisos, como moeda. Seu valor padrão é 0.0f.

  **Exemplo:** float f1 = 234.5f

- **Tipo double**

  O tipo de dado double é um ponto flutuante IEEE 754 de 64 bits de precisão dupla. Seu valor é ilimitado. O tipo de dado double é geralmente usado para valores decimais assim como float. O tipo de dado double também não deve ser usado para precisar valores, como moeda. Seu valor padrão é 0.0d.

  **Exemplo:** double d1 = 12.3

  ![](/home/andre/Downloads/java-data-types.png)

  ![](/home/andre/Downloads/datatype.jpeg)

###### Variáveis

Uma variável é um recipiente que contém o valor enquanto o **programa Java** é executado. Uma variável é atribuída com um tipo de dados.

Variável é um nome de alocação de memória. Existem três tipos de variáveis em java: local, global e estática.

Existem dois tipos de data em java: primitiva e não primitiva.

**Variável**

**Variável** é o nome de uma área reservada de alocação de memória. Em outras palavras, é um nome de memória alocada. É uma combinação de "vary + able" isso significa que seu valor pode ser modificado.

```
**int** idade = 35; //Aqui idade é uma variável
```

**Tipos de variáveis**

Existem três tipos de variáveis em java:

- **Variável local**

  Uma variável declarada dentro do corpo de um método é chamada de variável local. Você consegue usar esta variável apenas dentro do método e outro método na classe não terá conhecimento de que essa variável existe.
  Uma variável local não pode ser definida como a palavra reservada "static".

- **Variável global**

  Uma variável declarada dentro da classe mas fora do corpo de um método, é chamada variável global. Não é declarada como como static.

  É chamada variável global porque seu valor é especifico da instancia e não é compartilhado entre instâncias.
  
- **Variável estática**
  Uma variável que é considerada com static é chamada de variável estática. Ela não pode ser local. Você pode criar uma simples cópia de uma variável estática e compartilhar entre todas as instancias na classe. A alocação de memória para variáveis estáticas acontece apenas uma vez enquanto a classe é carregada em memória.

**Exemplo para entendermos os tipos de variáveis em java**

```
class A {  
   int idade = 35; //Variável global  
   static int mes = 10; // Variável estática  
   void metodo() {  
      int n = 90; // Variável local  
   }  
} // Fim da classe  
```

**Adicionando dois números**

```
class Simples {  
   public static void main(String[] args) {  
      int a = 10;  
      int b = 10;  
      int c = a + b;  
      System.out.println(c);    
   }  
}  
```

**Saída:**

```
20
```

**Extensão**

```
class Simples {  
   public static void main(String[] args) {  
      int a = 10;  
      float f = a;  
      System.out.println(a);  
      System.out.println(f);  
   }  
}  
```

**Saída:**

```
10
10.0
```

**Redução (conversão de tipo)**

```
class Simples{  
   public static void main(String[] args) {  
      float f = 10.5f;  
      //int a = f; //Erro em tempo de compilação  
      int a = (int) f;  
      System.out.println(f);  
      System.out.println(a);  
   }  
}  
```

**Saída:**

```
10.5
10
```

**Sobrecarga**

```
class Simples {  
   public static void main(String[] args) {  
      //Sobrecarga  
      int a = 130;  
      byte b = (byte) a;  
      System.out.println(a);  
      System.out.println(b);  
   }  
}  
```

**Saída:**

```
130
-126
```

**Adicionando tipo inferior**

```
class Simples {  
   public static void main(String[] args) {  
      byte a = 10;  
      byte b = 10;  
    //byte c = a + b; //Erro em tempo de compilação: porque a + b = 20 que será int  
      byte c = (byte) (a + b);  
      System.out.println(c);  
   }  
}  
```

**Saída:**

```
20
```

###### Constantes

Uma **constante** é um tipo especial de variável cujo valor, uma vez definido no código, não pode mais ser alterado durante a execução do programa.

Declaramos uma constante em Java usando a palavra-chave **final**, como mostram os exemplos a seguir:

```
public static final int TAXA = 20;
public static final float TAM_MIN = 20.50;
```

O código acima declara duas constantes, TAXA e TAM_MIN, contendo os valores imutáveis 20 e 20.50, respectivamente. Vejamos o que significam a palavras public, static e final:

***public\*** significa que as constantes estarão disponíveis (serão acessíveis) em todo o código do projeto

***static\*** indica que somente existirá uma cópia da constante compartilhada entre todas as instâncias de classe (o valor da constante será o mesmo não importa onde e quantas vezes apareça no código).

A palavra ***final*** significa que o valor atribuído não poderá ser alterado após a inicialização do elemento – ou seja, se torna um valor constante.

**Regras para nomeação de constantes**
Para nomear uma constante em Java, precisamos seguir algumas regras, listadas a seguir:

- Deve conter apenas letras, _ (underline), $ ou os números de 0 a 9
- Deve obrigatoriamente se iniciar por uma letra, _ ou $
- Não podemos usar palavras-chave da linguagem
- O nome deve ser único dentro de um escopo

*Boa prática*: Declarar sempre uma constante usando apenas letras maiúsculas, e em caso de palavras compostas, separá-las com um underline (_)

**Exemplos de nomes de constantes****Válidos:**

- NOME_CLIENTE
- TELEFONE_1
- PRECO_$

**Inválidos:**

- 1Telefone
- Nome Cliente
- $PRECO
  

**Código de exemplo**

```
public class constantes {

public static final double LARGURA = 10.0;
    public static void main(String[] args) {
        double compr = 25.0;
        double res = calculaArea(LARGURA, compr);
        System.out.println("A área é: " + res);
    }
    private static double calculaArea(double largura, double comprimento) {
        return largura * comprimento;
    }
}
```

Em nosso código de exemplo declaramos uma constante de nome LARGURA, do tipo double, que conterá o valor 10.0. Esse valor não poderá ser alterado durante a execução do programa.

Portanto, ao invocarmos o método ***calculaArea()\*** devemos informar o valor da largura passando a constante LARGURA e o valor de uma variável chamada de **compr**, que armazena um valor de comprimento. Assim, a área sempre será calculada com a mesma largura, variando apenas o comprimento usado no cálculo.

###### Exercícios

### Capítulo 5 - Introdução à API do Java

###### Composição

###### Organização

###### O Pacote Java

###### O Pacote Javax

###### Recursos Essenciais

###### Valores Aleatórios

###### Exercícios

### Capítulo 6 - Operadores

###### Conceito e Classificação

**Operador** em java é um símbolo que é usado para executar operações. Por exemplo: +, -, *, / etc.
Existem muitos tipos de operadores em Java que são fornecidos abaixo:

- Operador Unário,
- Operador aritmético,
- Operador shift,
- Operador relacional,
- Operador bit a bit (bitwise),
- Operador lógico,
- Operador ternário e
- Operador de atribuição.

**Precedência de operadores em Java**

![](/home/andre/Downloads/operadores.jpeg)

###### Operadores unário

Os operadores unários requerem apenas um operando. Operadores unários são usados para executar várias operações, ou seja:

- Incrementando / decrementando um valor em um
- Negando uma expressão
- Invertendo o valor de um boolean

**Exemplo de operador unário: ++ e --**

```
class OperadorExemplo {  
   public static void main (String args []) {  
      int x = 10;  
      System.out.println (x++); // 10 (11)  
      System.out.println (++x); // 12  
      System.out.println (x--); // 12 (11)  
      System.out.println (--x); // 10  
   }  
}  
```

**Saída:**

```
10
12
12
10
```

**Operador Unário Exemplo 2: ++ e --**

```
class OperadorExemplo {  
   public static void main (String args []) {  
      int a = 10;  
      int b = 10;  
      System.out.println (a++ + ++a); // 10 + 12 = 22  
      System.out.println (b++ + b++); // 10 + 11 = 21  
   }  
}  
```

**Saída:**

```
22
21
```

**Exemplo de operador unário: ~ e !**

```
class OperadorExemplo {  
   public static void main (String args []) {  
      int a = 10;  
      int b = -10;  
      boolean c = true;  
      boolean d = false;  
      System.out.println (~a); // - 11 (Inverte o sinal e subtrai com 1)  
      System.out.println (~b); // 9 (Inverte o sinal e subtrai com 1)  
      System.out.println (!c); // false (oposto ao valor booleano)  
      System.out.println (!d); // true  
   }  
}  
```

**Saída:**

```
-11
9
false
true
```

###### Operadores Aritméticos

Operadores aritméticos são usados para executar: adição, subtração, multiplicação e divisão. Eles agem como operações matemáticas básicas.

**Exemplo de operador aritmético**

```
class OperadorExemplo {  
   public static void main (String args[]) {   
      int a =10;  
      int b = 5;  
      System.out.println (a + b); // 15  
      System.out.println (ab); // 5  
      System.out.println (a * b); // 50  
      System.out.println (a / b); // 2  
      System.out.println (a % b); // 0  
   }  
}  
```

**Saída:**

```
15
5
50
2
0
```

**Exemplo de operador aritmético: Expressão**

```
class OperadorExemplo {  
   public static void main (String args[]) {  
      System.out.println(10 * 10 / 5 + 3 - 1 * 4 / 2);  
 }
}  
```

**Saída:**

```
21
```

**Operador Shift para a esquerda**

O operador Java left shift << é usado para mudar todos os bits de um valor para o lado esquerdo de um número especificado de vezes.
**Exemplo de operador Left Shift**

```
class OperadorExemplo {  
   public static void main (String args []) {  
      System.out.println (10 << 2); // 10 * 2 ^ 2 = 10 * 4 = 40  
      System.out.println (10 << 3); // 10 * 2 ^ 3 = 10 * 8 = 80  
      System.out.println (20 << 2); // 20 * 2 ^ 2 = 20 * 4 = 80  
      System.out.println (15 << 4); // 15 * 2 ^ 4 = 15 * 16 = 240  
   }  
}  
```

**Saída:**

```
40
80
80
240
```

**Operador Shift para a direita**
O operador Java right shift >> é usado para mover os operandos da esquerda para a direita pelo número de bits especificado pelo operando da direita.

**Exemplo de operador Right Shift**

```
class OperadorExemplo {  
   public static void main (String args []) {  
      System.out.println (10 >> 2); // 10 / 2 ^ 2 = 10 / 4 = 2  
      System.out.println (20 >> 2); // 20 / 2 ^ 2 = 20 / 4 = 5  
      System.out.println (20 >> 3); // 20 / 2 ^ 3 = 20 / 8 = 2  
   }  
}  
```

**Saída:**

```
2
5
2
```

**Exemplo do operador Shift: >> vs >>>**

```
class OperadorExemplo {  
   public static void main (String args []) {  
      // Para número positivo, >> e >>> funcionam da mesma forma  
      System.out.println (20 >> 2);  
      System.out.println (20 >>> 2);  
      // Para número negativo, >>> altera o bit de paridade (MSB) para 0  
     System.out.println (-20 >> 2);  
     System.out.println (-20 >>> 2);  
   }  
}  
```

**Saída:**

```
5
5
-5
1073741819
```

###### Operadores Relacionais

###### Operadores Lógicos

**Exemplo do operador AND: Lógico && e Bitwise &**

```
class OperadorExemplo {  
   public static void main (String args []) {  
      int a =10;  
      int b = 5;  
      int c = 20;  
      System.out.println (a < b && a < c); // falso && verdadeiro = falso  
      System.out.println (a < b & a < c); // falso & verdadeiro = falso  
   }  
}  
```

Saída:

```
false
false
```

**Exemplo do operador AND: Lógico && vs Bitwise &**

```
class OperadorExemplo {  
   public static void main (String args []) {  
      int a =10;  
      int b = 5;  
      int c = 20;  
      System.out.println(a < b && a++ < c); // falso && verdadeiro = falso  
      System.out.println(a); // 10 porque a segunda condição não está marcada  
      System.out.println(a < b & a++ < c); // falso && verdadeiro = falso  
      System.out.println(a); // 11 porque a segunda condição está marcada  
   }  
}  
```

Saída:

```
false
10
false 
11
```

**Exemplo de operador OR: Lógico || e bit a bit |**

```
class OperadorExemplo {  
   public static void main (String args []) {  
      int a = 10;  
      int b = 5;  
      int c = 20;  
      System.out.println(a > b || a < c); // true || true = true  
      System.out.println(a > b | a < c); // true | true = true  
      // ||vs |  
      System.out.println (a > b || a ++ < c); // true || true = true  
      System.out.println (a); // 10 porque a segunda condição não está marcada  
      System.out.println (a > b | a ++ < c); // true | true = true  
      System.out.println (a); // 11 porque a segunda condição está marcada  
   }  
}
```

Saída:

```
true
true
true
10
true
11
```

###### Operador Ternário

O operador ternário é usado como um substituto para a instrução if-then-else e muito usado na programação Java. É o único operador condicional que leva três operandos.

###### Procedencia Entre Operadores

**Exemplo de operador ternário Java**

```
class OperadorExemplo {  
   public static void main (String args []) {  
      int a = 2;  
      int b = 5;  
      int min = (a < b) ? a : b;  
      System.out.println(min);  
   }  
}  
```

Saída:

```
2
```

Outro exemplo:

```
class OperadorExemplo {  
   public static void main (String args []) {  
      int a = 10;  
      int b = 5;  
      int min = (a < b) ? a : b;  
      System.out.println (min);  
   }  
}  
```

Saída:

```
5
```

###### Operadores de Atribuição

O operador de atribuição é um dos operadores mais comuns. É usado para atribuir o valor à direita ao operando à esquerda.

**Exemplo de operador de atribuição**

```
class OperadorExemplo {  
   public static void main (String args []) {  
      int a = 10;  
      int b = 20;  
      a += 4; //a = a + 4 (a = 10 + 4)  
      b -= 4; //b = b - 4 (b = 20 - 4)  
      System.out.println(a);  
      System.out.println(b);  
   }  
}  
```

**Saída:**

```
14
16
```

**Exemplo de operador de atribuição Java**

```
class OperadorExemplo {  
   public static void main (String [] args) {  
      int a =10;  
      a += 3; // 10 + 3  
      System.out.println(a);  
      a -= 4; //13 - 4  
 System.out.println(a);  
 a *= 2; // 9 * 2  
 System.out.println(a);  
 a /= 2; // 18 / 2  
     System.out.println(a);  
   }  
}  
```

Saída:

```
13
9
18
9
```

**Exemplo de operador de atribuição: Adicionando short**

```
class OperadorExemplo {  
 public static void main (String args []) {  
  short a = 10;  
    short b = 10;  
     //a += b; //a = a + b internamente tem o mesmo resultado  
     a = a + b; // Erro em tempo de compilação porque 10 + 10 = 20 agora int  
     System.out.println(a);  
 }  
}  
```

Saída:

```
Erro de tempo de compilação
```

**Após alteração no tipo:**

```
class OperadorExemplo {  
 public static void main (String args []) {  
  short a =10;  
   short b =10;  
   a = (short) (a + b); // 20, que agora é convertido para short  
  System.out.println(a);  
 }  
}  
```

Saída:

```
20
```

###### Exercícios

### Capítulo 7 - Estrutura de Decisão

###### Introdução

A declaração if do java é usada para testar uma condição. Verifica condições boolean: true ou false. Existem vários tipos de declarações if em java.

- Declaração if
- Declaração if-else
- Declaração if-else-if encadeado
- Aninhamento de declarações if

###### Estrutura If

A declaração **if** em java testa uma condição.
A instrução condicional **if**  em Java tem por finalidade tomar uma decisão de acordo com o resultado de uma condição especificada (teste lógico).

**Sintaxe:**

```
if (teste_lógico) {
   Bloco de comandos a executar se o teste_lógico retornar verdadeiro
}
else {
   Bloco de comandos a executar se o teste_lógico retornar falso
}
```

O bloco ***else\*** pode ser omitido nos casos em que somente interessa executar alguma  ação quando o teste_lógico retornar verdadeiro, não importando o que  ocorra se o teste retornar falso.

**Exemplo:**

```
//Um programa java para demonstrar o uso da declaração if-else.
//Este é um programa que verifica números pares e impares
public class IfElseExemplo {
    public static void main(String[] args) {
        //definindo uma variável
        int numero = 13;
        //Verificando se o número é divisível por dois ou não
        if(numero % 2 == 0) {
            System.out.println("numero par");
        } else {
            System.out.println("numero impar");
        }
    }
}
```

**Saída:**

```
Idade é maior que 18 
```

###### Estrutura if-else

A declaração if-else em java também testa uma condição. Executa se a condição do bloco if é verdadeira caso contrário o bloco else é executado.

**Sintaxe:**

```
if(condicao) {  
   //código da condição if é verdadeira  
} else {  
   //código da condição if é falsa  
}  
```

**Exemplo:**

```
//Um programa java para demonstrar o uso da declaração if-else.
//Este é um programa que verifica números pares e impares
public class IfElseExemplo {
    public static void main(String[] args) {
        //definindo uma variável
        int numero = 13;
        //Verificando se o número é divisível por dois ou não
        if(numero % 2 == 0) {
            System.out.println("numero par");
        } eles {
            System.out.println("numero impar");
        }
    }
}
```

**Saída:**

```
numero impar
```

**Exemplo ano bissexto**

Um ano é bissexto, se ele é divisível por 4 e 400. Mas, não por 100.

```
public class AnoBissextoExemplo {
    public static void main(String[] args) {
        int ano = 2020;
        if (((ano % 4 == 0) && (ano % 100 != 0)) || (ano % 400 == 0)) {
            System.out.println("Ano Bissexto");
        }
        eles {
            System.out.println("Ano Comum");
        }
    }
}
```

Saída:

```
Ano Bissexto
```

**Usando Operador Ternário**
Nós podemos usar também o operador ternário (? :) para executar a tarefa da declaração if...else. É uma forma abreviada para checar a condição. Se a condição é verdadeira, o resultado da ? é retornada. Mas, se a condição é falsa, o resultado do : é retornado.

**Exemplo:**

```
public class IfElseTernarioExemplo {
    public static void main(String[] args) {
        int numero = 13;
        //Usando operador ternário  
        String saida = (numero % 2 == 0) ? "número par" : "número ímpar";
        System.out.println(saida);
    }
}
```

**Saída:**

```
número Ímpar
```

###### Estrutura switch

**Declaração Switch no Java**
A declaração switch executa uma declaração vinda de múltiplas condições. É como o if-else-if. Trabalha com byte, short, int, long, tipos enum, String e alguns outros tipos como Byte, Short, Int, and Long. Desde o Java 7, você pode usar strings na declaração switch.

Em outras palavras, a declaração switch testa a igualdade de uma variável mediante múltiplos valores.

**Pontos para relembrar**

- Pode haver um ou N números de valores case para uma expressão switch.
- O valor do case deve ser do mesmo tipo da expressão switch. O valor do case pode ser literal ou constante. Não permite variáveis.
- Os valores do case devem ser únicos. Em caso de valor duplicado, gera erro em tempo de compilação.
- O switch deve ser byte, short, int, long (com seus tipos não primitivos), enums e string.
- Cada declaração case pode ter um break que é opcional. Quando o controle atinge a declaração break, o controle pula para depois da expressão switch. Se o break não foi encontrado, é executado o próximo case.
- O case pode ter um valor default que é opcional.

**Sintaxe:**

```
switch(expression){
        case value1:
        //código para ser executado;
        break;  //opcional
        case value2:
        //código para ser executado;
        break;  //opcional
        ......

default:
        //código para ser executado se todos os cases não são aceitos;
        } 
```

![](/home/andre/Downloads/17be4aef-54c8-40ae-9548-d1702b2d5458.webp)

**Exemplo:**

```
public class SwitchExample {
    public static void main(String[] args) {
        //Declarando um variável para expressão switch   
        int number = 20;
        // Expressão Switch   
        switch (number) {
            //Declarações case   
            case 10:
                System.out.println("10");
                break;
            case 20:
                System.out.println("20");
                break;
            case 30:
                System.out.println("30");
                break;
            //Declaração default case   
            default:
                System.out.println("Not in 10, 20 or 30");
        }
    }
} 
```

**Saída:**

```
20
```

**Procurando Mês Exemplo:**

```
//Programa Java para demonstrar o exemplo de switch  
//Onde nós iremos imprimir o nome do mês para o número dado   
public class SwitchMesExemplo {
    public static void main(String[] args) {
        //Especificando número do mês
        int mes = 9;
        String mesString = "";
        //Declaração switch
        switch (mes) {
            //Declaração case dentro do bloco switch
            case 1:
                mesString = "1 - Janeiro";
                break;
            case 2:
                mesString = "2 - Fevereiro";
                break;
            case 3:
                mesString = "3 - Marco";
                break;
            case 4:
                mesString = "4 - Abril";
                break;
            case 5:
                mesString = "5 - Maio";
                break;
            case 6:
                mesString = "6 - Junho";
                break;
            case 7:
                mesString = "7 - Julho";
                break;
            case 8:
                mesString = "8 - Agosto";
                break;
            case 9:
                mesString = "9 - Setembro";
                break;
            case 10:
                mesString = "10 - Outubro";
                break;
            case 11:
                mesString = "11 - Novembro";
                break;
            case 12:
                mesString = "12 - Dezembro";
                break;
            default:
                System.out.println("Mes Invalido!");
        }
        //Escrevendo o mês de acordo com o número
        System.out.println(mesString);
    }
}
```

**Saída:**

```
9 - Setembro
```

**Declaração switch do java é simples**

Significa que ele executa todas as instruções após a primeira ocorrência se uma declaração de break não estiver presente

**Exemplo:**

```
//Exemplo switch em java onde nós omitimos a declaração break  
public class SwitchExemplo2 {
    public static void main(String[] args) {
        int number = 10;
        //expressão switch com valor int  
        switch (number) {
            //switch cases sem break  
            case 10:
                System.out.println("10");
            case 20:
                System.out.println("20");
            case 30:
                System.out.println("30");
            default:
                System.out.println("Não em 10, 20 ou 30");
        }
    }
}
```

**Saída:**

```
20
30
Não em 10, 20 ou 30
```

**Declaração Switch com String**
Java nos permite usar cadeias de caracteres na expressão switch desde o Java SE 7. A instrução case deve ser do mesmo tipo.

**Exemplo:**

```
//Programa Java para demonstrar o use do switch em Java  
//declaração com String  
public class SwitchStringExemplo {
    public static void main(String[] args) {
        //Declarando variável String
        String nivelString = "Pleno";
        int nivel = 0;
        //Usando String no switch
        switch (nivelString) {
            //Usando String no case
            case "Junior":
                nivel = 1;
                break;
            case "Pleno":
                nivel = 2;
                break;
            case "Senior":
                nivel = 3;
                break;
            default:
                nivel = 0;
                break;
        }
        System.out.println("Seu nível e: " + nivel);
    }
}
```

**Saída:**

```
Seu nível e: 2
```

**Declaração Switch Encadeado em Java**
Nós podemos usar switch dentro de outro switch em Java. Isso é conhecido como declaração switch encadeado.

**Exemplo:**

```
//Programa java para demonstrar o uso do switch encadeado  
public class EncadeadoSwitchExample {
    public static void main(String args[]) {
        //C - CSE, E - ECE, M - Mecânica   
        char ramo = 'C';
        int anoDaFaculdade = 4;
        switch (anoDaFaculdade) {
            case 1:
                System.out.println("Ingles, Matematica, Ciencia");
                break;
            case 2:
                switch (ramo) {
                    case 'C':
                        System.out.println("Sistemas Operacionais, Java, Estrutura de dados");
                        break;
                    case 'E':
                        System.out.println("Micro processadores, Teoria da Lógica de Programação");
                        break;
                    case 'M':
                        System.out.println("Desenho, Máquinas de Fabricação");
                        break;
                }
                break;
            case 3:
                switch (ramo) {
                    case 'C':
                        System.out.println("Organização Computacional, Multimédia");
                        break;
                    case 'E':
                        System.out.println("Fundamentos de Logica de Design, Microelectrônicos");
                        break;
                    case 'M':
                        System.out.println("Motores de Combustão Interna, Vibração  Mecânica");
                        break;
                }
                break;
            case 4:
                switch (ramo) {
                    case 'C':
                        System.out.println("Comunicação de dados e redes, Multimédia");
                        break;
                    case 'E':
                        System.out.println("Sistemas Embarcados, Processamento de Imagem");
                        break;
                    case 'M':
                        System.out.println("Produção Tecnológica , Energia Térmica");
                        break;
                }
                break;
        }
    }
}
```

**Saída:**

```
Comunicação de dados e redes, Multimédia
```

**Enum em Switch**
Java nos permite usar enum na instrução switch.

**Exemplo:**

```
//Programa Java para demonstrar o use de Enum  
//em uma declaração switch  
public class JavaSwitchEnumExemplo {
    public enum Dia {Dom, Seg, Ter, Qua, Qui, Sex, Sab}

    public static void main(String args[]) {
        Dia[] diaAgora = Dia.values();
        for (Dia agora : diaAgora) {
            switch (agora) {
                case Dom:
                    System.out.println("Domingo");
                    break;
                case Seg:
                    System.out.println("Segunda");
                    break;
                case Ter:
                    System.out.println("Terça");
                    break;
                case Qua:
                    System.out.println("Quarta");
                    break;
                case Qui:
                    System.out.println("Quinta");
                    break;
                case Sex:
                    System.out.println("Sexta");
                    break;
                case Sab:
                    System.out.println("Sabado");
                    break;
            }
        }
    }
}
```

**Saída:**

```
Domingo
Segunda
Terça
Quarta
Quinta
Sexta
Sabado
```

**Variáveis não primitivas com Switch**
Java oferece quatro tipos de classes não primitivas: Byte, Short, Integer e Long em instruções switch.

**Exemplo:**

```
//Programa java  para demonstrar o uso de classes não primitivas  
//em instruções switch  
public class NaoPrimitivoInSwitchCaseExample {
    public static void main(String args[]) {
        Integer idade = 84;
        switch (idade) {
            case (16):
                System.out.println("Você tem menos de 18.");
                break;
            case (18):
                System.out.println("Você é elegível por voto.");
                break;
            case (65):
                System.out.println("Você é idoso.");
                break;
            default:
                System.out.println("Por favor forneça uma idade válida.");
                break;
        }
    }
}
```

**Saída:**

```
Por favor forneça uma idade válida.
```

###### Exercícios

### Capítulo 8 Estrutura de Repetição

###### Introdução

Em linguagens de programação, loops são usados para executar uma sequencia de instruções / funções repetidamente quando algumas condições tornam-se true.
Existem três tipos de loops em java.

- Estrutura for

- Estrutura while
- Estrutura do-while

|         Comparação         |                        Estrutura for                         |                       Estrutura while                        |                      Estrutura do while                      |
| :------------------------: | :----------------------------------------------------------: | :----------------------------------------------------------: | :----------------------------------------------------------: |
|         Introdução         | O for é uma declaração de fluxo de controle que itera parte dos programas várias vezes. | O while é uma declaração de fluxo de controle que executa parte dos programas repetidamente com base na condição boolean especificada. | O do while é uma instrução de fluxo de controle que executa uma parte dos programas pelo menos uma vez e a execução posterior depende da condição boolean especificada. |
|        Quando usar         | Se o número de iterações for fixo, é recomendável usar o for. | Se o número de iterações não for fixo, é recomendável usar o while. | Se o número de iterações não for fixo e você precisar executar o loop pelo menos uma vez, é recomendável usar o do while. |
|          Sintaxe           | `for (inicialização; condição; incr / decr) { // código a ser executado }` |       `while (condição) { // código a ser executado }`       |     `do{ // código a ser executado } while (condição);`      |
|          Exemplo           | `// loop for   for (int i = 1; i <= 10; i ++) {   System.out.println (i);   }` | `// loop while   int i = 1;   while (i <= 10) {   System.out.println(i);   i++;   }` | `// loop do-while   int i = 1;   do{   System.out.println (i);   i++;   } while (i <= 10);` |
| Sintaxe para loop infinito |         `para(;;){   // código a ser executado   }`          |       `while (true) {   // código a ser executado   }`       | `do{   // código a ser executado   } while (verdadeiro);`  |

###### Estrutura for

A estrutura de repetição "**for**", ou então, laço de repetição, iterador é a estrutura mais utilizada quando precisamos executar diversas vezes um  mesmo bloco de código. Isso porque, conseguimos facilmente declarar,  inicializar e incrementar valores no cabeçalho da estrutura. Logo, nós  temos que o nosso código acaba por ficar encapsulado, o que confere  segurança na execução do laço como também, uma melhor legibilidade do  código e maior produtividade

Existem três tipos de loops for em java.

- Simples loop for
- For-each (foreach) ou for melhorado
- Loop for marcado

**Simples loop for**

Um simples loop for é o que utilizado no C / C++. Nós podemos inicializar a variável, checar a condição e incrementar ou decrementar o valor. Consiste de quatro partes:

- **Inicialização:** É a condição inicial que é executada uma vez quando o loop começa. Aqui, nós podemos inicializar a variável, ou nós podemos usar uma variável já inicializada. É uma condição opcional.
- **Condição:** É a segunda condição que é executada todo tempo para testar a condição no loop. Continua a execução até a condição seja falsa. Ela deve retornar um valor booleanosendo true ou false. É uma condição opcional.
- **Declaração:** A declaração do loop é executada cada vez até a segunda opção ser falsa.
- **Incremento / decremento:** Ele incrementa ou decrementa o valor da variável. Está é uma condição opcional.

**Sintaxe:**

```
for(inicialização; condição; incremento/decremento) {  
     //declaração ou código que será executado  
}  
```

**Exemplo:**

```
//Programa Java para demonstrar o exemplo do loop for
//Que imprime a tabela de 1   
public class ExemploFor {
    public static void main(String[] args) {
        //Código do loop for em java  
        for (int i = 1; i <= 10; i++) {
            System.out.println(i);
        }
    }
}
```

**Saída:**

```
1
2
3
4
5
6
7
8
9
10
```

**Loop for encadeado**
Se nós temos um loop for dentro de outro loop, ele é conhecido como loop for encadeado. O loop interno executa completamente sempre que o loop externo é executado.

**Exemplo:**

```
public class ExemploForEncadeado {
    public static void main(String[] args) {
        //loop de i
        for (int i = 1; i <= 3; i++) {
            //loop de j
            for (int j = 1; j <= 3; j++) {
                System.out.println(i + " " + j);
            } // fim de j
        } // fim de i
    }
}
```

**Saída:**

```
1 1
1 2
1 3
2 1
2 2
2 3
3 1
3 2
3 3
```

**Loop for-each em java**
O loop for-each é usado para atravessar arrays ou coleções em java. É fácil para usar do que o loop for simples porque não precisamos aumentar o valor e usa notação subscrita.

Ele trabalha com elementos básicos não indexados. Retorna um por um dos elementos definidos na variável.

**Sintaxe:**

```
for(Tipo da variável variável : array) {  
    //código que será executado  
}  
```

**Exemplo:**

```
//Exemplo de loop for-each em java que escreve os elementos do array
public class ForEachExemplo {
    public static void main(String[] args) {
        //declaração do array
        int arr[] = {21, 33, 37, 48, 84};
        //Imprimindo array usando loop for-each
        for (int i : arr) {
            System.out.println(i);
        }
    }
} 
```

**Saída:**

```
21
33
37
48
84
```

**Loop for marcado em java**

Nós podemos ter um nome de cada loop for do java. Para fazer isso, nós usamos um rotulo antes do loop for. Isto é útil se nós temos um loop for encadeado em que nós podemos parar ou continuar algum loop for especifico.

Geralmente, parar e continuar são as palavras reservadas break e continue, usados na parte mais interna do loop.

**Sintaxe:**

```
labelNome:  
for(inicialização; condição; incr/decr) {  
    //código que será executado  
}  
```

**Exemplo:**

```
//Um programa java para demonstrar o uso do loop for marcado
public class ExemploForMarcado {
    public static void main(String[] args) {
        //Usando rotulo para for externo e loop for
        aa:
        for (int i = 1; i <= 3; i++) {
            bb:
            for (int j = 1; j <= 3; j++) {
                if (i == 2 && j == 2) {
                    break aa;
                }
                System.out.println(i + " " + j);
            }
        }
    }
}
```

**Saída:**

```
1 1
1 2
1 3
2 1
```

Se você usa **break bb;**, Ele irá parar dentro do loop interno, que é o comportamento padrão de qualquer loop.

```
public class ExemploForMarcado2 {
    public static void main(String[] args) {
        //Usando rotulo para for externo e loop for  
        aa:
        for (int i = 1; i <= 3; i++) {
            bb:
            for (int j = 1; j <= 3; j++) {
                if (i == 2 && j == 2) {
                    break bb;
                }
                System.out.println(i + " " + j);
            }
        }
    }
}
```

**Saída:**

```
1 1
1 2
1 3
2 1
3 1
3 2
3 3
```

**Loop for infinito em java**

Se você usar dois ponto e vírgula ;; no loop for, ele será um loop infinito.

**Sintaxe:**

```
for(;;) {  
    //código que será executado  
}
```

**Exemplo:**

```
//Programa java para demonstrar o uso do loop infinito for que escreve uma declaração
public class ForExemplo {
    public static void main(String[] args) {
        //Usando nenhuma condição no loop for  
        for (; ; ) {
            System.out.println("loop infinito");
        }
    }
}
```

**Saída:**

```
loop infinito
loop infinito
loop infinito
loop infinito
loop infinito
loop infinito
loop infinito
loop infinito
ctrl+c //para o loop
```

###### Estrutura While

O loop while em java é usado para percorrer uma parte do programa algumas vezes. Se o número de iterações não é fixado, é recomendado o uso do loop while.

**Sintaxe:**

```
while(condição) {  
    //código que será executado  
}
```

**Exemplo:**

```
public class ExemploWhile {
    public static void main(String[] args) {
        int i = 1;
        while (i <= 10) {
            System.out.println(i);
            i++;
        }
    }
}
```

**Saída:**

```
1
2
3
4
5
6
7
8
9
10
```

**Loop infinito usando o while**
Se você passar **true** no loop while, ele será executado infinitamente.

**Sintaxe:**

```
while(true) {  
   //código que será executado  
}  
```

**Exemplo:**

```
public class ExemploWhile2 {
    public static void main(String[] args) {
        while (true) {
            System.out.println("loop while infinito");
        }
    }
}
```

**Saída:**

```
loop infinito
loop infinito
loop infinito
loop infinito
loop infinito
loop infinito
loop infinito
loop infinito
ctrl+c //para o loop
```

###### Estruturado **do** while

O loop do while do java é usado para repetir uma parte do programa algumas vezes. Se o número de iterações não é fixo e você precisa executar o loop pelo menos uma vez. É recomendado o uso do loop do while.

O loop do while é executado pelo menos uma vez porque a condição é checada depois do corpo do loop.

**Sintaxe:**

```
do {  
//código que será executado  
} while(condição);  
```

**Example:**

```
public class DoWhileExemplo {
    public static void main(String[] args) {
        int i = 1;
        do {
            System.out.println(i);
            i++;
        } while (i <= 10);
    }
}
```

**Saída:**

```
1
2
3
4
5
6
7
8
9
10
```

**Loop infinito do while**
Se você passar **true** em um loop do while, se tornará infinito.

**Sintaxe:**

```
do{  
   //código que será executado  
} while(true);
```

**Example:**

```
public class DoWhileExemplo2 {
    public static void main(String[] args) {
        do {
            System.out.println("do while loop infinito");
        } while (true);
    }
}
```

**Saída:**

```
do while loop infinito
do while loop infinito
do while loop infinito
do while loop infinito
do while loop infinito
do while loop infinito
ctrl+c
```

###### Quebra de Laço

Quando uma declaração break é encontrada dentro de um loop, o loop é terminado imediatamente e o controlador do programa prossegue a próxima declaração seguido o loop.

O break é usado para parar uma declaração switch ou um loop. Onde é parado o fluxo atual em uma condição especifica do programa.

Nós podemos usar a declaração break em todos os tipos de loops como por exemplo: for, while e do while.

**Sintaxe:**

```
declaracaoQueSeraSaltada;  
break;
```

**Declaração Break utilizando loop**

**Exemplo:**

```
package EstruturaLoop;

//Programa java para demostrar o uso da declaração break dentro do loop for
public class ExemploBreak {
    public static void main(String[] args) {
        //Usando loop for
        for (int i = 1; i <= 10; i++) {
            if (i == 8) {
                //parando o loop
                break;
            }
            System.out.println(i);
        }
    }
}
```

**Saída:**

```
1
2
3
4
5
6
7
```

**Break com loop interno**

**Exemplo:**

```
//Programa java para ilustrar o uso da declaração break dentro de um loop interno
public class BreakExemplo2 {
    public static void main(String[] args) {
        //loop externo
        for (int i = 1; i <= 3; i++) {
            //loop interno
            for (int j = 1; j <= 3; j++) {
                if (i == 2 && j == 2) {
                    //Usando a declaração break dentro de um loop interno
                    break;
                }
                System.out.println(i + " " + j);
            }
        }
    }
}
```

**Saída:**

```
1 1
1 2
1 3
2 1
3 1
3 2
3 3
```

**Break com loop for marcado**
Nós podemos usar a declaração break com um marcador. Este recurso foi introduzido desde o JDK 1.5. Então, nós podemos utilizar o break em qualquer loop seja interno ou externo.

**Exemplo:**

```
//Programa java para ilustrar o uso do break
//Com o marcador dentro de um loop interno para parar o loop externo  
public class BreakExemplo3 {
    public static void main(String[] args) {
        aa:
        for (int i = 1; i <= 3; i++) {
            bb:
            for (int j = 1; j <= 3; j++) {
                if (i == 2 && j == 2) {
                    //usando break com marcador
                    break aa;
                }
                System.out.println(i + " " + j);
            }
        }
    }
}
```

**Saída:**

```
1 1
1 2
1 3
2 1
```

**Break em um loop while**

**Exemplo:**

```
//Programa java para demonstrar o uso do break dentro do loop while
public class BreakWhileExemplo {
    public static void main(String[] args) {
        //loop while
        int i = 1;
        while (i <= 10) {
            if (i == 5) {
                //usando o break
                i++;
                break; //O loop irá parar
            }
            System.out.println(i);
            i++;
        }
    }
}
```

**Saída:**

```
1
2
3
4
```

**Break no loop do-while**

**Exemplo:**

```
//Programa java para demostrar o uso da declaração break
//dentro loop do while.   
public class BreakDoWhileExemplo {
    public static void main(String[] args) {
        //Declaração de variável
        int i = 1;
        //loop do while
        do {
            if (i == 5) {
                //usando o break
                i++;
                break; //O loop será parado
            }
            System.out.println(i);
            i++;
        } while (i <= 10);
    }
} 
```

**Saída:**

```
1
2
3
4
```

###### Exercícios

### Capítulo 9 - Vetores e Matrizes

###### Introdução

Declarando e manipular vetores e matrizes na linguagem Java, além disso, vamos compreender seu funcionamento interno para tirar melhor proveito desse recurso. Vamos entendemos qual a necessidade da utilização de variáveis na programação. Porém, em softwares que possuem grandes quantidades de dados similares fica muito cansativo e inviável a declaração de uma variável para cada dado. Pensando nisso foram criados os agrupamentos de variáveis denominados vetores.

Vetores são estruturas que armazenam uma quantidade fixa de dados de um certo tipo. Por esta razão, esse tipo de estrutura também é conhecida como estrutura homogênea de dados.

Em um vetor, os espaços capazes de armazenar dados são indexados por índices que posteriormente servem como endereço para acesso.

Cada item de um **array** é chamado de elemento, e cada elemento é acessado pelo número, o  índice. Abaixo é mostrado se dá esse acesso aos seus elementos,  lembrando que sempre sua numeração começa em 0.

![Array de 5 posiçoes](/home/andre/Downloads/ArraysEmJava1.jpg)

###### Vetores

Os vetores podem ser manipulados de forma direta, ou seja, é possível ler os dados contidos nele ou gravar diretamente sem nenhuma regra ou restrição. Outra característica dos vetores é que o número de posições disponíveis é igual ao tamanho definido pelo programador. Por exemplo: Um vetor declarado com tamanho 10, possuirá 10 posições e pode armazenar até 10 elementos distintos. Os índices dos vetores são formados por números inteiros, positivos, linear e sequencialmente enumerados. [ 0, 1, 2, 3, 4, … n ]. É interessante ressaltar que as linguagens C, C++, Java, entre outras, iniciam seus vetores em 0.

**Declarando Arrays**

Na declaração de um array, cada elemento recebe um valor padrão, sendo 0 (**zero**) para números de tipo primitivo, falso (**false**) para elementos booleanos e nulo (**null**) para referências. O programa que está na Listagem 1, cria um array de  inteiros, coloca alguns valores nela, e imprime cada valor à saída  padrão.

**Exemplo:**

```
package Array;

public class Array1 {
    public static void main(String[] args) {

        //DECLARA UM ARRAY DE INTEIROS
        int[] meuArray;
        //ALOCA MEMÓRIA PARA 10 INTEIROS
        meuArray = new int[10];

        //INICIALIZA O PRIMEIRO ELEMENTO
        meuArray[0] = 1;
        meuArray[1] = 2;
        meuArray[2] = 3;
        meuArray[3] = 4;
        meuArray[4] = 5;
        meuArray[5] = 6;
        meuArray[6] = 7;
        meuArray[7] = 8;
        meuArray[8] = 9;
        meuArray[9] = 0;
        //ESTOURA A PILHA POIS NÃO EXISTE O ÍNDICE 10

        System.out.println(meuArray[9]);
        System.out.println(meuArray[2]);
    }
}
```

**Saída:**

```
0
3
```

**tamanho de um array**
Por padrão, cada array sabe seu próprio tamanho, independente de quantos valores forem inseridos. O array armazena na variável de instância o  método length, que retorna o tamanho do array especificado.

Exemplo:

```
public class TamanhoArray {

    public static void main(String[] args) {
        int[] arrayUm = {1, 3, 5, 8, 9, 6, 7, 4, 33, 2, 10};
        int[] arrayDois = {9, 19, 4, 7, 37, 21, 2, 48, 4};

        if (arrayDois.length > 8) {
            System.out.println("Tamanho do ArrayDois - Maior que 8!");
        } else {
            System.out.println("Tamanho do ArrayDois - Menor que 8!");
        }
        System.out.println("\nTamanho do ArrayUm = " + arrayUm.length);
    }
}
```

Saĺda:

```
Tamanho do ArrayDois - Maior que 8!

Tamanho do ArrayUm = 11
```

**Inicializando um array**

Quando inicializamos um array no momento da declaração, é vista uma  lista de valores separados por vírgula dispostos entre chaves { }.

Exemplo:

```
public class InicializandoArray {
    public static void main(String[] args) {
        //ARRAY COM 10 ELEMENTOS
        int[] arrayBase;
        arrayBase = new int[10];
        System.out.printf("%s %10s \n", "Index", "Valores");
        //GERA A SAÍDA DO VALOR DE CADA ELEMENTO DO ARRAY
        for (int i = 0; i < arrayBase.length; i++)
            System.out.printf("%3d %10d \n", i, arrayBase[i]);
    }
}
```

Saída:

```
Index    Valores 
  0          0 
  1          0 
  2          0 
  3          0 
  4          0 
  5          0 
  6          0 
  7          0 
  8          0 
  9          0 
```

Exemplo:

```
public class Inicializando_Array {
    public static void main(String[] args) {
        //LISTA  DE VALORES
        int[] array = {10, 20, 30, 40, 50, 60, 70, 80, 90, 100};
        System.out.printf("%s %12s \n", "Index", "Valores");
        //PERCORRE CADA ELEMENTO E IMPRIME O ÍNDICE COM O VALOR
        for (int counter = 0; counter < array.length; counter++) {
            System.out.printf("%5d %4s %4d \n", counter, "=>", array[counter]);
        }
    }
}
```

Saída:

```
Index      Valores 
    0   =>   10 
    1   =>   20 
    2   =>   30 
    3   =>   40 
    4   =>   50 
    5   =>   60 
    6   =>   70 
    7   =>   80 
    8   =>   90 
    9   =>  100
```

**Percorrendo Arrays**

A instrução **for aprimorado**, como é conhecida, serve para obter elementos de um array, sem a possibilidade de alterá-los. Se caso  houver necessidade de modificar os elementos, isso pode ser feito com a  instrução **for controlada por contador**.

Na sintaxe do for aprimorado é divida pelos seguintes parâmetros:

- O tipo com um identificador (parâmetro);
- O nome do array no qual serão feitas as iterações.

Sintaxe:

```
for ( parâmetro : nomeDoArray )
    instrução
```

Exemplo:

```
public class Percorrendo_Arrays_For_Aprimorado {
    public static void main(String[] args) {
        int[] arrayNum = {1, 2, 3, 4, 5, 6, 7, 8};
        int total = 0;
        //ADICIONA O VALOR DE CADA ELEMENTO AO TOTAL
        for (int i : arrayNum)
            total += i;
        System.out.printf("Soma dos elementos arrayNum: %d\n", total);
    }
}
```

###### Matrizes

O Java não suporta nativamente vetores multidimensionais, então a solução adotada é utilizar vetores de vetores. Para declarar um vetor multidimensional em Java, deve-se seguir a seguinte sintaxe:
Nesse caso, os dados são armazenados em índice baseado em linha e coluna (também conhecido como forma de matriz).

**Sintaxe para declarar matriz multidimensional em Java**

```
dataType[][] arrayRefVar;
dataType [][]arrayRefVar;
array de dataTypeRefVar[];;
dataType []arrayRefVar[];   
```

**Exemplo para instanciar matriz multidimensional em Java**

```
int[][] arr=novo int[3][3]; //3 linha e 3 colunas
```

**Exemplo para inicializar matriz multidimensional em Java**

```
arr[0][0]=1;  
arr[0][1]=2;  
arr[0][2]=3;  
arr[1][0]=4;  
arr[1][1]=5;  
arr[1][2]=6;  
arr[2][0]=7;  
arr[2][1]=8;  
arr[2][2]=9;  
```

**Exemplo de Java Array Multidimensional**
Vejamos o exemplo simples para declarar, instanciar, inicializar e imprimir a matriz 2Dimensional.

```
//Java Program to illustrate the use of multidimensional array
public class Testarray3 {
    public static void main(String args[]) {
//declaring and initializing 2D array  
        int arr[][] = {{1, 2, 3}, {2, 4, 5}, {4, 4, 5}};
//printing 2D array  
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(arr[i][j] + " ");
            }
            System.out.println();
        }
    }
} 
```

Saída:

```
1 2 3 
2 4 5 
4 4 5 
```

**Arrays multidimensionais**

Esse tipo de array é declarado como tendo duas dimensões e é usado  para representar tabelas de valores que consistem em informações  organizadas em linhas e colunas.

Os **arrays bidimensionais** precisam de dois índices para identificar um elemento particular.

Por exemplo, quando um array é identificado dessa forma "numero[indiceA][indiceB]", a variável **numero** é o array, o **indiceA** é a linha e o **indiceB** é identificado como a coluna, fazendo uma identificação de cada elemento no array por número de linha e coluna.

Exemplo de declaração: **int [][] a = { { 1, 2 }, { 2, 2 } };**

![](/home/andre/Downloads/ArraysEmJava2.png)

**Exemplo:**

```
public class ArraysBidimensionais {
    public static void main(String[] args) {

        int[][] array1 = {{1, 2, 3}, {4, 5, 6}};
        int[][] array2 = {{1, 2}, {3}, {4, 5, 6}};

        System.out.println("Valores no array1 passados na linha são");
        outputArray(array1); //exibe o array 2 por linha

        System.out.println("Valores no array2 passados na linha são");
        outputArray(array2); //exibe o array 2 por linha

    }
    //FAZ UM LOOP PELAS LINHAS DO ARRAY
    public static void outputArray(int[][] array) {
        //FAZ UM LOOP PELAS COLUNAS DA LINHA ATUAL
        for (int linha = 0; linha < array.length; linha++) {
            //FAZ LOOP PELAS COLUNAS DA LINHA ATUAL
            for (int coluna = 0; coluna < array[linha].length; coluna++)
                System.out.printf("%d ", array[linha][coluna]);
            System.out.println();
        }
    }
}
```

Saída:

```
Valores no array1 passados na linha são
1 2 3 
4 5 6 
Valores no array2 passados na linha são
1 2 
3 
4 5 6
```

###### Exercícios

### Capítulo 10 - Tratamento de Exceções

###### Introdução

###### Estrutura try-catch

###### Bloco Finaly

###### A Hierarquia das Exceções

###### Exercícios

### Capítulo 11 - Conceito de Orientação a Objeto

###### Introdução

###### Objetos

###### Classes

###### Operadores de Abstração

###### Classificação e instanciação

###### Generalização e Especialização

###### Agregação e Decomposição

###### Exercícios

### Capítulo 12 - Anatomia de Classe

###### Introdução

###### Declaração de Classe

###### Instanciação de Classe

###### Atributos

###### Construtores

###### Métodos

###### Unindo as Partes

###### Exercícios

### Capítulo 13 - Encapsulamento

###### Introdução

###### Atributos Públicos e Quebra de Integridade de Conteúdo

###### Bloqueio de Acesso Externo aos Atributos

###### Método de Leitura e Escrita

###### Validaçôes nos Métodos de Escrita

###### Uso de Unchecked Exceptions nos Métodos de Escrita

###### Uso do Checked Exceptions nos Métodos de Escrita

###### Reresentação Textual Atraves do Método toString()

###### Validação no Métodos no Construtor

###### Relação Assimétrica de Atributos com Métodos de Leitura e de Escrita

###### Atributos e Métodos Estáticos

###### Atributoa Constantes

###### Métodos de Leitura Para Atributos Booleanos

###### Exercícios

### Capítulo 14 - Herança

###### O Conceito de Herança

**Herança em Java** é um mecanismo no qual um objeto adquire todas as propriedades e comportamentos de um objeto pai. É uma parte importante do **OOPs**(sistema de programação orientado a objetos).

A ideia por trás da herança em Java é que você pode criar novas classes que são **construídas**

sobre as classes existentes. Quando você herda de uma classe existente, você pode reutilizar métodos e campos da classe dos pais. Além disso, você pode adicionar novos métodos e campos em sua classe atual também.

A herança representa a **relação IS-A**, que também é conhecida como uma relação ***pai-filho***.

**Por que usar herança em java**

- Para o método **overriding** (para que o polimorfismo do tempo de execução possa ser alcançado).
- Para reusability de código.

**Termos usados em Herança**

- **Class:** Uma classe é um grupo de objetos que têm propriedades comuns. É um modelo ou projeto a partir do qual os objetos são criados.
- **Sub Class/ Child Class:** Subclass é uma classe que herda a outra classe. Também é chamada de classe derivada, classe estendida ou classe filha.
- **Super Class/Parent Class:** Superclass é a classe de onde uma subclasse herda as características. Também é chamada de classe base ou classe de pais.
- **Reutilização:** Como o nome especifica, a reutilização é um mecanismo que facilita a reutilização dos campos e métodos da classe existente quando você cria uma nova classe. Você pode usar os mesmos campos e métodos já definidos na classe anterior.

**A sintaxe da Herança Java**

```
class Subclas-name extends Superclass-name
{  
   métodos e campos  
}  
```

A **palavra-chave extends** indica que você está fazendo uma nova classe que deriva de uma classe existente. O significado de "alonga" é aumentar a funcionalidade.

Na terminologia de Java, uma classe que é herdada é chamada de pai ou superclasse, e a nova classe é chamada de filha ou subclasse.

```
public class Funcinario {
    float salario = 50000;
}

class Programador extends Funcinario {
    int bonus = 10000;

    public static void main(String args[]) {
        Programador p = new Programador();
        System.out.println("O salario do programador é:" + p.salario);
        System.out.println("Bonus do Programador é:" + p.bonus);
    }
} 
```

**Saída:**

```
O salario do programador é:50000.0
Bonus do Programador é:10000
```

No exemplo acima, o objeto Programador pode acessar o campo da própria classe, bem como da classe Employee, ou seja, reutilização de código.

**Tipos de herança em java**

<img src="/home/andre/Downloads/typesofinheritance.jpg" style="zoom:80%;" />

Quando uma classe herda várias classes, é conhecida como herança múltipla. Por exemplo:

<img src="/home/andre/Downloads/multiple.jpg" style="zoom:80%;" />

**Exemplo de herança única**

Quando uma classe herda outra classe, é conhecida como uma *única herança*. No exemplo abaixo, a classe Cachorro herda a classe Animal, então há a herança única.

```
public class Animal {
    void comer() {
        System.out.println("comendo...");
    }
}

class Cachorro extends Animal {
    void latir() {
        System.out.println("latindo...");
    }
}

class TestHeranca {
    public static void main(String args[]) {
        Cachorro c = new Cachorro();
        c.latir();
        c.comer();
    }
} 
```

**Saída:**

```
latindo...
comendo...
```

**Exemplo de herança multinível**
Quando há uma cadeia de herança, é conhecida como *herança multinível*. Como você pode ver no exemplo dado abaixo, a classe BabyDog herda a classe Dog que herda novamente a classe Animal, então há uma herança multinível.

```
public class Animal2 {
    void comer() {
        System.out.println("comendo...");
    }
}

class Cachorro1 extends Animal2 {
    void latir() {
        System.out.println("latindo...");
    }
}

class CachorroFilho extends Cachorro1 {
    void chorar() {
        System.out.println("chorando...");
    }
}
```

**Saída:**

```
chorando...
latindo...
comendo...
```


**Exemplo de herança hierárquica**
Quando duas ou mais classes herdam uma única classe, é conhecida como *herança hierárquica*. No exemplo dado abaixo, as classes Cachorro e Gato herda a classe Animal, por isso há herança hierárquica.

```
public class Animal3 {
    void comer() {
        System.out.println("comendo...");
    }
}

class Cachorro3 extends Animal3 {
    void latir() {
        System.out.println("latindo...");
    }
}

class Gato extends Animal3 {
    void miar() {
        System.out.println("miando...");
    }
}

class TestHeranca3 {
    public static void main(String args[]) {
        Gato g = new Gato();
        g.miar();
        g.comer();
    }
}
```

**Saída:**

```
miando...
comendo...
```


**Java não suporta herança mútipla**

Considere um cenário onde A, B e C são três classes. A classe C herda as classes A e B. Se as classes A e B tiverem o mesmo método e você chamá-lo de objeto de classe infantil, haverá ambiguidade para chamar o método de classe A ou B.

Como os erros de tempo de compilação são melhores do que erros de tempo de execução, o Java renderiza o erro de tempo de compilação se você herdar 2 classes. Então, se você tem o mesmo método ou diferente, haverá erro de tempo de compilação.


**herança múltipla não é suportada em java**

```
public class A {
    void msg() {
        System.out.println("Olá");
    }
}

class B {
    void msg() {
        System.out.println("Bem-Vindo");
    }
}

class C extends A, B {//suponha se fosse

    public static void main(String args[]) {
        C obj = new C();
        obj.msg();//Agora qual método msg() seria invocado?
    }
}
```

Saída:

```
 Compile Time Error
```


**Agregação em Java**

Se uma classe tem uma referência de entidade, é conhecida como Agregação. A agregação representa a relação HAS-A.

Considere uma situação, o objeto do Funcionário contém muitas informações como id, nome, emailId etc. Ele contém mais um objeto chamado endereço, que contém suas próprias informações, como cidade, estado, país, cepa etc. como dado abaixo.

```
class Funcionario{  
int id;  
String name;  
Endereco endereco;//Endereco é uma classe
...  
}  
```

Nesse caso, o Funcionario tem um endereço de referência da entidade, por isso o relacionamento é endereoo do Funcionario.


**Por que usar agregação?**

- Para reusability de código.
  

**Exemplo simples de agregação**

Neste exemplo, criamos a referência da classe Operação na classe Círculo.

```
class Operacao {
    int quadrado(int n) {
        return n * n;
    }
}

class Circulo {
    Operacao op;//aggregação
    double pi = 3.14;

    double area(int raio) {
        op = new Operacao();
        int rquadrado = op.quadrado(raio);//code reusability (i.e. delegates the method call).
        return pi * rquadrado;
    }

    public static void main(String args[]) {
        Circulo c = new Circulo();
        double result = c.area(3);
        System.out.println(result);
    }
}
```

Saída:

```
28.26
```


**Quando usar agregação?**

- O reaproveitamento de código também é melhor alcançado pela agregação quando não há relação é-a.
- A herança só deve ser usada se a relação é mantida ao longo da vida dos objetos envolvidos; caso contrário, a agregação é a melhor escolha.


**Entendendo exemplo significativo de Agregação**

Neste exemplo, o Funcionário tem um objeto de Endereço, o objeto de endereço contém suas próprias informações, como cidade, estado, país etc. Nesse caso, o relacionamento é endereço HAS-A do Empregado.

**Endereco.java**

```
public class Endereco {
    String cidade, estado, pais;

    public Endereco(String cidade, String estado, String pais) {
        this.cidade = cidade;
        this.estado = estado;
        this.pais = pais;
    }
}
```

**Funcionario.java**

```
public class Funcionario {
    int id;
    String name;
    Endereco endereco;

    public Funcionario(int id, String name, Endereco endereco) {
        this.id = id;
        this.name = name;
        this.endereco = endereco;
    }

    void display() {
        System.out.println(id + " " + name);
        System.out.println(endereco.cidade + " " + endereco.estado + " " + endereco.pais);
    }

    public static void main(String[] args) {
        Endereco endereco1 = new Endereco("São Paulo", "SP", "Brasil");
        Endereco endereco2 = new Endereco("Croata", "CE", "Brasil");

        Funcionario e = new Funcionario(111, "André", endereco1);
        Funcionario e2 = new Funcionario(112, "Renato", endereco2);

        e.display();
        e2.display();

    }
} 
```

**Saída:**

```
111 André
São Paulo SP Brasil
112 Renato
Croata CE Brasil
```

### O Conceito de Polimorfismo

**Polimorfismo em Java: o que é, pra que serve, como e onde usar**

Agora que aprendemos os conceitos mais importantes e vimos o **uso da Herança em Java**, vamos estudar outra características marcante da programação Java e, de uma maneira mais geral, da programação orientada a objetos: o **polimorfismo**.**

** Com o polimorfismo vamos ter um controle maior sobre as subclasses sem ter que nos preocupar especificamente com cada uma delas, pois cada uma terá autonomia para agir de uma maneira diferente.

**Definição de polimorfismo em Java**

Traduzindo, do grego, ao pé da letra, polimorfismo significa "muitas formas".**
** Essas formas, em nosso contexto de programação, são as subclasses/objetos criados a partir de uma classe maior, mais geral, ou abstrata.**
** Polimorfismo é a capacidade que o Java nos dá de controlar todas as formas de uma maneira mais simples e geral, sem ter que se preocupar com cada objeto especificamente.

###### Sobrecarga de Métodos (**Overloading**)

Se uma **classe** tem múltiplos métodos com o mesmo nome, mas diferentes em parâmetros, é conhecida como **Sobrecarga de Método**.

Se tivermos que realizar apenas uma operação, ter o mesmo nome dos métodos aumenta a legibilidade do **programa**.

Suponha que você tenha que executar a adição dos números dados, mas pode haver qualquer número de argumentos, se você escrever o método como a (int,int) para dois parâmetros, e b (int,int,int) para três parâmetros, então pode ser difícil para você, bem como outros programadores entender o comportamento do método porque seu nome difere.

Então, executamos a sobrecarga de métodos para descobrir o programa rapidamente.


**Vantagem da sobrecarga de métodos**

A sobrecarga do método *aumenta a legibilidade do programa*.


**Diferentes maneiras de sobrecarregar o método**

Há duas maneiras de sobrecarregar o método em java

- Mudando o número de argumentos
- Alterando o tipo de dados
  


**Vantagem da sobrecarga de métodos**

A sobrecarga do método *aumenta a legibilidade do programa*.


**Diferentes maneiras de sobrecarregar o método**

Há duas maneiras de sobrecarregar o método em java

- Mudando o número de argumentos
- Alterando o tipo de dados


**Sobrecarga do método: alteração não. de argumentos**

Neste exemplo, criamos dois métodos, primeiro método de adição () realiza a adição de dois números e o segundo método de adição realiza a adição de três números.

Neste exemplo, estamos criando **métodos estáticos**

para que não precisemos criar exemplo para chamar métodos.

```
class Adiciona {
    static int add(int a, int b) {
        return a + b;
    }

    static int add(int a, int b, int c) {
        return a + b + c;
    }
}

class TestOverloading1 {
    public static void main(String[] args) {
        System.out.println(Adiciona.add(11, 11));
        System.out.println(Adiciona.add(11, 11, 11));
    }
}
```

**Saída:**

```
22
33
```

**Sobrecarga de método: alterando o tipo de dados de argumentos**

Neste exemplo, criamos dois métodos que diferem no **tipo de dados**. O primeiro método de adoção recebe dois argumentos inteiros e o segundo método de adoção recebe dois argumentos duplos.

```
class Adiciona1 {
    static int add(int a, int b) {
        return a + b;
    }

    static double add(double a, double b) {
        return a + b;
    }
}

class TestOverloading2 {
    public static void main(String[] args) {
        System.out.println(Adiciona1.add(11, 11));
        System.out.println(Adiciona1.add(12.3, 12.6));
    }
}
```

**Saída:**

```
22
24.9
```

**Por que a sobrecarga de métodos não é possível alterando apenas o tipo de método de retorno?**

Em java, a sobrecarga do método não é possível alterando o tipo de retorno do método apenas por causa da ambiguidade. Vamos ver como a ambiguidade pode ocorrer:

```
class Adiciona2 {
    static int add(int a, int b) {
        return a + b;
    }

    static double add(int a, int b) {
        return a + b;
    }
}

class TestOverloading3 {
    public static void main(String[] args) {
        System.out.println(Adiciona2.add(11, 11));//ambiguity
    }
} 
```

**Saída:**

```
Compile Time Error: method add(int,int) is already defined in class Polimorfismo.Adiciona2
```

System.out.println(Adder.add(11,11)); Aqui, como java pode determinar qual método de soma () deve ser chamado?

**Podemos sobrecarregar o método java main() ?**

Sim, por sobrecarga de métodos. Você pode ter qualquer número de métodos principais em uma classe por sobrecarga de método. Mas o **JVM** chama o método principal() que recebe matriz de strings apenas como argumentos. Vejamos o exemplo simples:

```
class TestOverloading4 {
    public static void main(String[] args) {
        System.out.println("Principal com String[]");
    }

    public static void main(String args) {
        System.out.println("Principal com String");
    }

    public static void main() {
        System.out.println("principal sem args");
    }
}
```

**Saída:**

```
Principal com String[]
```

**Sobrecarga de métodos e promoção de tipo**
Um tipo é promovido a outro implicitamente se não for encontrado um tipo de dados correspondente. Vamos entender o conceito pela figura dada abaixo:

<img src="/home/andre/Downloads/java-type-promotion.png" style="zoom:80%;" />

Como mostrado no diagrama acima, o byte pode ser promovido a curto, int, longo, flutuante ou duplo. O tipo de dados curto pode ser promovido a int, longo, flutuante ou duplo. O tipo de dados char pode ser promovido a int, longo, flutuar ou dobrar e assim por diante.
andre ribeiro

**Exemplo de sobrecarga de método com tipo promoção**

```
class CalculationSobrecarga {
    void sum(int a, long b) {
        System.out.println(a + b);
    }

    void sum(int a, int b, int c) {
        System.out.println(a + b + c);
    }

    public static void main(String args[]) {
      CalculationSobrecarga obj = new CalculationSobrecarga();
        obj.sum(19, 48);//agora segundo int literal será promovido a longo
        obj.sum(7, 9, 1);

    }
}
```

**Saída:**

```
67
17
```

**Exemplo de sobrecarga de método com promoção de tipo se correspondência encontrada**

Se houver argumentos de tipo correspondentes no método, a promoção do tipo não será realizada.

```
class CalculationSobrecarga2 {
    void sum(int a, int b) {
        System.out.println("int arg method invocado");
    }

    void sum(long a, long b) {
        System.out.println("método arg longo invocad");
    }

    public static void main(String args[]) {
        CalculationSobrecarga2 obj = new CalculationSobrecarga2();
        obj.sum(21, 37);//agora int arg sum() método é invocado
    }
}
```

**Saída:**

```
int arg method invocado
```

**Sobrecarga de Método com Promoção de Tipo em caso de ambiguidade**

Se não houver argumentos de tipo de correspondência no método, e cada método promover um número semelhante de argumentos, haverá ambiguidade.

```
class CalculationSobrecarga3 {
    void sum(int a, long b) {
        System.out.println("a método invocado");
    }

    void sum(long a, int b) {
        System.out.println("b método invocado");
    }

    public static void main(String args[]) {
        CalculationSobrecarga3 obj = new CalculationSobrecarga3();
        obj.sum(20, 20);//agora ambiguidade
    }
} 
```

**Saída:**

```
both method sum(int,long) in Polimorfismo.CalculationSobrecarga3 and method sum(long,int) in Polimorfismo.CalculationSobrecarga3 match
```

<!--Um tipo não é despromissado implicitamente, por exemplo, o duplo não pode ser rebaixado a qualquer tipo implicitamente.-->



###### Métodos sobreposto (Overriding)

Se a subclasse (classe menor) tiver o mesmo método declarado na classe dos pais, é conhecida como **Overriding de método em Java**.

Em outras palavras, se uma subclasse fornece a implementação específica do método que foi declarado por uma de suas classes parentais, é conhecido como sobreponto de método.


**Utilização do Método Overriding Java**

- A substituição do método é usada para fornecer a implementação específica de um método que já é fornecido por sua superclasse.
- A substituição do método é usada para polimorfismo em tempo de execução


**Regras para substituição de método java**

- O método deve ter o mesmo nome da classe dos pais
- O método deve ter o mesmo parâmetro da classe dos pais.
- Deve haver uma relação IS-A (herança).


**Entender o problema sem sobrepor método**
Vamos entender o problema que podemos enfrentar no programa se não usarmos o método de substituição.

```
/Programa Java para demonstrar por que precisamos de sobreponderação de métodos
//Aqui, estamos chamando o método de classe dos pais com o filho
//objeto de classe.
//Criando uma classe de pai
class Veiculo {
    void correr() {
        System.out.println("Veiculo está em movimento!!!");
    }
}

//Criando uma classe filha
class Bicicleta extends Veiculo {
    public static void main(String args[]) {
        //criando uma instância de classe filha
        Bicicleta obj = new Bicicleta();
        //chamando o método com a instância de classe filha 
        obj.correr();
    }
}
```

**Saída:**

```
Veiculo está em movimento!!!
```

O problema é que eu tenho que fornecer uma implementação específica do método correr() na subclasse, por isso usamos a substituição do método.


**Exemplo de sobreponderação de métodos**

Neste exemplo, definimos o método de execução na subclasse definida na classe pai, mas tem alguma implementação específica. O nome e o parâmetro do método são os mesmos, e há relação IS-A entre as classes, por isso há o método overrriding.

```
//Programa Java para ilustrar o uso de Java Method Overriding
//Criando uma classe de pais.
class Veiculo2 {
    //  definindo um método
    void correr() {
        System.out.println("Veiculo está em movimento");
    }
}

//Criando uma classe filha
class Bike2 extends Veiculo2 {
    //definindo o mesmo método que na classe dos pais
    void run() {
        System.out.println("Bike está em movimento com segurança");
    }

    public static void main(String args[]) {
        Bike2 obj = new Bike2();//criar objeto
        obj.correr();//método de chamada
    }
}
```

**Saída:**

```
Bike está em movimento com segurança
```



**Podemos substituir o método estático?**
É porque o método estático está vinculado à classe, enquanto o método de instância está vinculado a um objeto. Estática pertence à área de classe, e uma instância pertence à área do monte.

**Podemos substituir o método principal java?**
Não, porque o principal é um método estático.



**Diferença entre o método overloading e overriding**

| Nº   | Métodos **Overloading**                                      | Métodos **Overriding**                                       |
| :--- | :----------------------------------------------------------- | :----------------------------------------------------------- |
| 1)   | A sobrecarga do método é usada *para aumentar a legibilidade* do programa. | A substituição do método é usada *para fornecer a implementação específica* do método que já é fornecido por sua super classe. |
| 2)   | A sobrecarga do método é realizada *dentro da aula*.         | A substituição do método ocorre *em duas classes* que têm relação IS-A (herança). |
| 3)   | Em caso de sobrecarga de método, *o parâmetro deve ser diferente*. | Em caso de substituição do método, *o parâmetro deve ser o mesmo*. |
| 4)   | A sobrecarga de métodos é o exemplo de *compilar o polimorfismo do tempo*. | A substituição do método é o exemplo do *polimorfismo do tempo de execução*. |
| 5)   | Em java, a sobrecarga do método não pode ser realizada alterando apenas o tipo de retorno do método. *O tipo de retorno pode ser o mesmo ou diferente* na sobrecarga de métodos. Mas você deve ter que mudar o parâmetro. | *O tipo de retorno deve ser o mesmo ou covariante* na substituição do método. |



**Exemplo de sobrecarga de método Java**

```
class OverloadingExamplo {
    static int add(int a, int b) {
        return a + b;
    }

    static int add(int a, int b, int c) {
        return a + b + c;
    }
} 
```



**Exemplo de sobreponderação do método Java**

```
class Animal {
    void comer() {
        System.out.println("Comendo...");
    }
}

class Cachorro extends Animal {
    void comer() {
        System.out.println("Comendo Pão ...");
    }
}
```





###### Tipo de retorno covariante

O tipo de retorno covariante especifica que o tipo de retorno pode variar na mesma direção que a subclasse.

Antes do Java5, não era possível substituir qualquer método alterando o tipo de retorno. Mas agora, desde Java5, é possível substituir o método alterando o tipo de retorno se a subclasse substituir qualquer método cujo tipo de retorno não-primitivo, mas muda seu tipo de retorno para tipo de subclasse. Vamos dar um exemplo simples:


**Exemplo simples de Tipo de Retorno covariante**

```
class A {
    A get() {
        return this;
    }
}

class B1 extends A {
    @Override
    B1 get() {
        return this;
    }

    void message() {
        System.out.println("Bem-vindo para tipo de retorno covariante!!");
    }

    public static void main(String args[]) {
        new B1().get().message();
    }
}
```

**Saída:**

```
Bem-vindo para tipo de retorno covariante!!
```

Como você pode ver no exemplo acima, o tipo de retorno do método get() da classe A é A, mas o tipo de retorno do método get() da classe B é B. Ambos os métodos têm tipo de retorno diferente, mas é sobreponto de método. Isso é conhecido como tipo de retorno covariante.



**Vantagens do Tipo de Retorno Covariante**

A seguir estão as vantagens do tipo de retorno covariante.

1) O tipo de retorno covariante ajuda a ficar longe dos moldes do tipo confuso na hierarquia de classe e torna o código mais utilizável, legível e mantido.

2) No método de substituição, o tipo de retorno covariante proporciona a liberdade de ter mais aos tipos de retorno de ponto.

3) O tipo de retorno covariante ajuda a prevenir o tempo de execução *classCastExceptions* em devoluções.

Vamos dar um exemplo para entender as vantagens do tipo de retorno covariante.

```
class A1 {
    A1 foo() {
        return this;
    }

    void print() {
        System.out.println("Dentro da classe A1");
    }
}

// A2 é a classe filha de A1
class A2 extends A1 {
    @Override
    A1 foo() {
        return this;
    }

    void print() {
        System.out.println("Dentro da classe A2");
    }
}

// A3 é a classe filha de A2
class A3 extends A2 {
    @Override
    A1 foo() {
        return this;
    }

    @Override
    void print() {
        System.out.println("Dentro da classe A3");
    }
}

public class CovariantExample {
    // Método main
    public static void main(String argvs[]) {
        A1 a1 = new A1();

        // Está ok
        a1.foo().print();

        A2 a2 = new A2();

        // Precisamos fazer o type casting para 
        // deixar mais claro sobre o tipo de objeto criado!!
        ((A2) a2.foo()).print();

        A3 a3 = new A3();

        // Fazendo o type casting
        ((A3) a3.foo()).print();
    }
}
```

**Saída:**

```
Dentro da classe A1
Dentro da classe A2
Dentro da classe A3
```



**Explicação:** No programa acima, a classe A3 herda a classe A2, e a classe A2 herda a classe A1. Assim, a A1 é a mãe das classes A2 e A3. Portanto, qualquer objeto das classes A2 e A3 também é do tipo A1. Como o tipo de retorno do método *foo()* é o mesmo em todas as classes, não sabemos o tipo exato de objeto que o método está realmente retornando. Só podemos deduzir que o objeto devolvido será do tipo A1, que é a classe mais genérica. Não podemos dizer com certeza que o objeto devolvido será de A2 ou A3. É onde precisamos fazer a digitação para descobrir o tipo específico de objeto retornado do método *foo()*. Ele não só faz o código verbose; também requer precisão do programador para garantir que a digitação seja feita corretamente; caso contrário, há chances justas de obter o *ClassCastException*. Para exacerbar, pense em uma situação em que a estrutura hierárquica desceu para 10 - 15 classes ou até mais, e em cada classe, o método *foo()* tem o mesmo tipo de retorno. Isso é o suficiente para dar um pesadelo ao leitor e escritor do código.

A melhor maneira de escrever o acima é:

```
class A1 {
    A1 foo() {
        return this;
    }

    void print() {
        System.out.println("Dentro da classe A1");
    }
}

// A2 classe filha de A1 
class A2 extends A1 {
    @Override
    A2 foo() {
        return this;
    }

    void print() {
        System.out.println("Dentro da classe A2");
    }
}

// A3 classe filha de A2
class A3 extends A2 {
    @Override
    A3 foo() {
        return this;
    }
    @Override
    void print() {
        System.out.println("Dentro da classe A3");
    }
}
public class CovariantExamplo {
    // Método main
    public static void main(String argvs[]) {
        A1 a1 = new A1();

        a1.foo().print();

        A2 a2 = new A2();

        a2.foo().print();

        A3 a3 = new A3();

        a3.foo().print();
    }
}
```

**Saída:**

```
Dentro da classe A1
Dentro da classe A2
Dentro da classe A3
```



**Explicação:** No programa acima, não é necessária a digitação, pois o tipo de retorno é específico. Portanto, não há confusão sobre saber o tipo de objeto que é devolvido do método *foo()*. Além disso, mesmo se escrevermos o código para as classes 10- 15, não haveria confusão em relação aos tipos de retorno dos métodos. Tudo isso é possível por causa do tipo de retorno covariante.



**Como os tipos de retorno do Covariant são implementados?**

Java não permite a sobrecarga baseada no tipo de retorno, mas o JVM sempre permite a sobrecarga baseada no tipo de retorno. JVM usa a assinatura completa de um método para busca/resolução. A assinatura completa significa que inclui o tipo de retorno, além dos tipos de argumentos. ou seja, uma classe pode ter dois ou mais métodos diferentes apenas por tipo de retorno. javac usa este fato para implementar tipos de retorno covariante.

**Explicação:** Para cada número de 1 a 20, o método *éPowerfulNo()* é invocado com a ajuda de for-loop. Para cada número, um *vetor primeFactors* é criado para armazenar seus divisores primos. Em seguida, verificamos se o quadrado de cada número presente no *vetor primeFactors* divide o número ou não. Se todo o quadrado de todo o número presente no *vetor primeFactors* dividir o número completamente, o número é um número poderoso; caso contrário, não.

###### Super Palavra-chave em Java

A **super** palavra-chave em Java é uma variável de referência que é usada para consultar objeto de classe pai imediata.

Sempre que você cria a instância da subclasse, uma instância da classe pai é criada implicitamente que é referida por uma variável de super referência.

**Uso da super palavra-chave Java**

1. super pode ser usado para encaminhar variável de instância de classe pai imediata.

2. super pode ser usado para invocar método de classe pai imediato.

3. super() pode ser usado para invocar o construtor de classe pai imediato.

   

**1) super é usado para referir variável de instância de classe pai imediata.**
Podemos usar a palavra-chave para acessar o membro de dados ou o campo da classe dos pais. É usado se a classe dos pais e a classe infantil têm os mesmos campos.

```
class Animal2 {
    String color = "Branco";
}

class Cao extends Animal2 {
    String color = "Preto";

    void printColor() {
        System.out.println(color);//imprime a cor da classe ao
        System.out.println(super.color);//imprime cor da classe Animal
    }
}

class TestSuper1 {
    public static void main(String args[]) {
        Cao d = new Cao();
        d.printColor();
    }
}
```

**Saída:**

```
Preto
Branco
```

No exemplo acima, Animal e Dog ambas as classes têm uma cor de propriedade comum. Se imprimirmos a propriedade colorida, ela imprimirá a cor da classe atual por padrão. Para acessar a propriedade dos pais, precisamos usar a palavra-chave.



**2) super pode ser usado para invocar o método de classe dos pais**
A super palavra-chave também pode ser usada para invocar o método de classe dos pais. Deve ser usado se a subclasse contiver o mesmo método da classe dos pais. Em outras palavras, é usado se o método for substituído.

```
class Animal {
    void comer() {
        System.out.println("comendo...");
    }
}

class Cao extends Animal {
    void comer() {
        System.out.println("comendo pão ...");
    }

    void latir() {
        System.out.println("latindo...");
    }

    void trabalhar() {
        super.comer();
        latir();
    }
}

class TestSuper2 {
    public static void main(String args[]) {
        Cao d = new Cao();
        d.trabalhar();
    }
}
```

**Saída:**

```
comendo...
latindo...
```

No exemplo acima Animal e Cão ambas as classes têm método de comer () se chamarmos de método comer() da classe Dog, ele chamará o método de comer() da classe Dog por padrão porque a prioridade é dada ao local.

Para chamar o método de classe dos pais, precisamos usar a palavra-chave.



**3) super é usado para invocar o construtor de classe dos pais.**
A super palavra-chave também pode ser usada para invocar o construtor de classe pai. Vejamos um exemplo simples:

```
lass Animal {
    Animal() {
        System.out.println("animal está criado");
    }
}

class Dog extends Animal {
    Dog() {
        super();
        System.out.println("cão está criado");
    }
}

class TestSuper3 {
    public static void main(String args[]) {
        Dog d = new Dog();
    }
} 
```

**Saída:**

```
animal está criado
cão está criado
```

**Nota**: super() é adicionado em cada construtor de classe automaticamente pelo compilador se não houver super() ou isso().




Como sabemos bem, o construtor padrão é fornecido pelo compilador automaticamente se não houver construtor. Mas, também adiciona super() como a primeira declaração.

**Outro exemplo de super palavra-chave onde super() é fornecido pelo compilador implicitamente.**

```
class Animal {
    Animal() {
        System.out.println("animal está criado");
    }
}

class Dog extends Animal {
    Dog() {
        System.out.println("cão está criado");
    }
}

class TestSuper4 {
    public static void main(String args[]) {
        Dog d = new Dog();
    }
}
```

**Saída:**

```
animal está criado
cão está criado
```

**super exemplo: uso real**
Vamos ver o uso real da palavra-chave. Aqui, a classe Emp herda a classe Pessoa para que todas as propriedades do Person sejam herdadas ao Emp por padrão. Para inicializar toda a propriedade, estamos usando o construtor de classe dos pais da classe infantil. Dessa forma, estamos reutilizando o construtor da classe dos pais.

```
class Pessoa {
    int id;
    String nome;

    Pessoa(int id, String name) {
        this.id = id;
        this.nome = name;
    }
}

class Funcionario extends Pessoa {
    float salario;

    Funcionario(int id, String nome, float salario) {
        super(id, nome);//reusing parent constructor
        this.salario = salario;
    }

    void display() {
        System.out.println(id + " " + nome + " " + salario);
    }
}

class TestSuper5 {
    public static void main(String[] args) {
        Funcionario func1 = new Funcionario(1, "andre", 45000f);
        func1.display();
    }
}
```

**Saída:**

```
1 andre 45000.0
```



###### Bloco de inicialização de instância

**O bloco de inicialização de** instâncias é usado para inicializar o membro de dados de instância. Ele é executado cada vez quando o objeto da classe é criado.
A inicialização da variável instância pode ser feita diretamente, mas pode ser realizada operações extras ao inicializar a variável instância no bloco inicializador de instância.

Qual é o uso do bloco de inicialização de instância enquanto podemos atribuir diretamente um valor no membro de dados de instância.

**Exemplo:**

```
class Bike{
	int veloc=100;
}
```



**Por que usar o bloco de inicialização de instâncias?**
Suponha que eu tenha que executar algumas operações ao atribuir valor ao membro de dados da instância, por exemplo, um loop para preencher um array complexo ou manipulação de erros etc.

**Exemplo de bloco de inicialização de instância**

Vejamos o simples exemplo do bloco de inicialização de instâncias que executa a inicialização.

```
class Bike7 {
    int speed;

    Bike7() {
        System.out.println("A velociadade é: " + speed);
    }

    {
        speed = 100;
    }

    public static void main(String args[]) {
        Bike7 b1 = new Bike7();
        Bike7 b2 = new Bike7();
    }
}
```

**Saída:**

```
A velociadade é: 100
A velociadade é: 100
```


Há três lugares em java onde você pode realizar operações:

1. method

2. constructor

3. block

   

**O que é invocado em primeiro lugar, bloco de inicialização de instância ou construtor?**

```

```

**Saída:**

```

```


No exemplo acima, parece que o bloco inicialização de instâncias é primeiro invocado, mas NÃO. O bloco de intialização de instância é invocado no momento da criação do objeto. O compilador java copia o bloco de inicialização da instância no construtor após a primeira instrução super(). Então, em primeiro lugar, o construtor é invocado. Vamos entendê-lo pela figura abaixo:

Nota: O compilador java copia o código do bloco inicializador de instância em cada construtor.

<img src="/home/andre/Downloads/instanceinitializerblock.jpg" style="zoom: 67%;" />

###### Estudo de Caso: Folha de Pagamento

###### Exercícios

### Capítulo 15 - Interfaces

###### Conceito

###### Atributos e Métodos

###### A Declaração Implements

###### Herança e Polimorfismo

###### Estudo de Caso: Cálculo de Área em Figuras Geométricas

###### Estudo de Caso: Cálculos de Tributos Para Produtos

###### Estudo de Caso: Constantes Para Mapear Estados Civis

###### Interfaces que Compõem a API do java

###### Exercícios

### Capítulo 16 - Pacotes

###### Conceito

###### A Declaração Package

###### A Declaração import

###### Exercícios

### Capítulo 17 - Recursos especiais do Java

###### Introdução

###### Comentários de Documentação

###### Classes Internas

###### Arquivos com Multiplas Classes

###### Objetos Anônimos

###### Importação Estáticas

###### Estrutura de Repetição foreach

###### Autoboxing

###### Qunatidade de Variável de Argumentos na Inovação de Métodos

###### Enumerções

###### Métodos Genéricos

###### Classes Genéricas

###### Exercícios
